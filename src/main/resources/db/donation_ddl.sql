CREATE DATABASE if not exists acms;
use acms;
CREATE TABLE if not exists `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) DEFAULT NULL,
  `status` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `zip` varchar(30) DEFAULT NULL,
   `age` int(3) DEFAULT NULL,
  `street_address` varchar(200) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `last_updated_by` varchar(50) DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'Admin',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE if not exists `donation_record` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `donation_amt` decimal(10,2) NOT NULL DEFAULT '0.00',
  `donation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `donation_type` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `country` varchar(100) NOT NULL DEFAULT '',
  `state` varchar(100) NOT NULL DEFAULT '',
  `city` varchar(100) NOT NULL DEFAULT '',
  `zip` varchar(50) DEFAULT NULL,
  `phone` varchar(20) NOT NULL DEFAULT '',
  `tshirt_size` varchar(100) DEFAULT NULL,
  `effort` varchar(20) DEFAULT '1',
  `payment_type` varchar(50) DEFAULT NULL,
  `mailing_id` varchar(50) DEFAULT NULL,
  `report_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/**create user and grant**/
GRANT USAGE ON *.* TO 'donation'@'%' IDENTIFIED BY PASSWORD '*6561028F9B88123E8CA43FCC2DFFEAB64573BA19';
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON `acms`.`user` TO 'donation'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES ON `acms`.`donation_record` TO 'donation'@'%';