﻿<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<META content="MSHTML 6.00.2800.1276" name=GENERATOR>
		<META http-equiv=Content-Type content="text/html; charset=UTF-8">
		<META http-equiv=Content-Style-Type content=text/css>
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<title>ACMS System Login</title>
		<LINK href="<%=request.getContextPath()%>/css/login.css" type=text/css rel=stylesheet>
		<script language='javascript' src='<%=request.getContextPath()%>/js/jquery.js'></script>
		<script language="javascript">
			$(document).ready(function() {
				getTdHeight();
				getBrowser();
			});
			function getBrowser() {
				if ($.browser.msie) {
					$(".browesPosition").css("position", "absolute");
				} else {
					$("div").removeClass("browesPosition");
				}
			}

			function getTdHeight() {
				tdheight = $(window).height();
				$("#toptd").css('height', tdheight * 0.2);
				$("#tableheight").css('height', tdheight);
			}

			//取得风格
			function getStyle(obj) {
				document.getElementById("useStyle").value = obj.id;
				submitButton('STYLE');
			}
			function login(){
	            document.forms['fm-user'].submit();
	        }
		</script>
	</head>


	<body LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 CLASS=bg_1>
	
	<form method="post" name="fm-user" id="fm-user" action="<%=request.getContextPath()%>/user/login.do">
		<DIV CLASS=browesPosition>
			<TABLE WIDTH=100% BORDER=0 CELLSPACING=0 CELLPADDING=0 ID=tableheight>				
				<TR>
					<TD ID=middletd>
						<DIV CLASS=bg_middle>
							<TABLE ALIGN=CENTER>
								<TR>
									<TD>
										<DIV CLASS=login>
											<DIV CLASS=login_L>
												<DIV CLASS=login_1>
													<ul>
														<LI><em CLASS=time>
														  <script language="javascript">
														    var today = new Date();
															var dateStr = (today.getMonth()+1) + "/"  + today.getDate() + "/" + today.getFullYear();
														    document.writeln("Today is "+dateStr);
														  </script>
														</em></LI>
														
														<LI CLASS=l_sty_1>
															<p ID=l_y_y_1><img id="selectedLanguageSrc" src="images/login/login_y_y_CN.png" />
																<em>English</em></p>
															<DL ID=l_y_y_2 STYLE="display:none;">
																<dd id="CN"><img src="images/login/login_y_y_CN.png" />
																		<em>English</em></dd>
															</DL>
														</LI>
													</ul>
												</DIV>
												<DIV CLASS=login_2>
													<strong>Usage Remind</strong>
													<p>This system is developed by XXXX!</p>
												</DIV>
											</DIV>
											<DIV CLASS=login_R>
                                                <span class="l_logo">ACMS System</span>												
												<ul>													
													<LI><SPAN>User:</SPAN>
														<input size="20" class="TextField" id="userName" type="text" value="admin" name="userName" /></LI>
													<LI><SPAN>Password:</SPAN>
														<input size="20" class="TextField" id="password" type="password" name="password" value="123456"/></LI>
													<LI></LI>
													<LI><SPAN>&nbsp;</SPAN><img onclick="login();" border="0" style="cursor:pointer" src="images/login/userLogin_button.png" />
														</LI>
												</ul>																		
										</DIV>
										</DIV>
									</TD>
								</TR>
							</TABLE>
						</DIV>
					</TD>
				</TR>
				<TR>
					<TD CLASS=bg_foot ALIGN=CENTER>
						<DIV CLASS=login_3>
							2014-05-15 版权所有 xxxx团队&nbsp;&nbsp;<span>版本号:V1.0</span>
						</DIV>
					</TD>
				</TR>
			</TABLE>
		</DIV>
	</form></body>
</html>