<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%> 
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>CCHR International The Mental Health Watchdog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/widgets.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/themes.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/select2.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/icheck/skins/square/blue.css" rel="stylesheet">

	<style>
		body{
			font-family : "Trebuchet MS", "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size: 13pt;
		}
		.theme-default{
			background-color: #fff;
		}
		.row {
			margin-left: 0;
			margin-right: 0;
		}
		.progress {
			height: 3px;
		}
		.headline {
			font-size : 23pt;
		}
		.topheaderline {
			font-family : "Times New Roman", "Trebuchet MS", "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size : 37pt;
			color: #000;
		}
		.sectionhead {
			font-size : 20pt;
		}
		
		.desc {
			font-size : 15pt;
		}
		.card-div{
			float:left;
			margin-right:15px;
		}
		.credit-card-black
		{
			background:url(${pageContext.request.contextPath}/assets/images/cchr/creditCardType-black.png);
			background-repeat:no-repeat;
			cursor:pointer;
		}
		
		.credit-card
		{
			background:url(${pageContext.request.contextPath}/assets/images/cchr/creditCardType.png);
			background-repeat:no-repeat;
			cursor:pointer;
		}
		
		.master
		{
			background-position:0px 0px;
			height:60px;
			width:95px;
		}
		
		.visa
		{
			background-position:-95px 0px;
			height:60px;
			width:95px;
		}
		
		.american-express
		{
			background-position:-190px 0px;
			height:60px;
			width:95px;
		}
		
		.discover
		{
			background-position:-285px 0px;
			height:60px;
			width:95px;
		}
	</style>
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/images/cchr/logo.png">
	<!--[if lt IE 9]>
		<script src="${pageContext.request.contextPath}/assets/javascripts/ie.min.js"></script>
	<![endif]-->
</head>
<body class="theme-default main-menu-animated">

<script>var init = [];</script>

<tiles:insertAttribute name="header" />

<form class="form-horizontal" action="${pageContext.request.contextPath}/donate" method="post" modelAttribute="donationRecord" id="jq-validation-form">
	<div class="row" style=";font-weight: bold">
		<div class="container marketing" style="color:#000;">
		
			<tiles:insertAttribute name="donationAmt" />
			
			<tiles:insertAttribute name="donationGift" />
			
			<tiles:insertAttribute name="monthlyDonation" />
			
			<tiles:insertAttribute name="billingAddress" />
			
			<tiles:insertAttribute name="paymentInformation" />
			
			<tiles:insertAttribute name="commit" />
			
		</div>
	</div>
	<input type="hidden" name="effort" value="${effort }">
	<input type="hidden" name="mailingID" value="${mailingID }">
	<input type="hidden" name="reportID" value="${reportID }"> 
</form>

<div class="row" style="margin-top:30px;margin-bottom:30px">
	<div class="container marketing" style="height:50px">

	  <!-- Three columns of text below the carousel -->
	  <div class="row" style="text-align:center;color:#000;font-size:14px">
		<p>&copy 2015 Citizens Commission on Human Rights International. All Rights Reserved.</p>
<p>6616 Sunset Blvd., Los Angeles, CA 90028</p>
<p>323-467-4242 / 800-869-2247</p>
		
	  </div>
</div>

<div class="row" style="margin-top:30px;margin-bottom:30px">
	
</div>
<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="${pageContext.request.contextPath}/assets/javascripts/jquery-2.0.3.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="${pageContext.request.contextPath}/assets/javascripts/jquery-1.8.3.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="${pageContext.request.contextPath}/assets/javascripts/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/javascripts/pixel-admin.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/javascripts/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/icheck/icheck.min.js"></script>

<script type="text/javascript">
	init.push(function () {
		
		$('input[name="donationAmt"]').iCheck({
		    checkboxClass: 'icheckbox_square-blue',
		    radioClass: 'iradio_square-blue',
		    increaseArea: '20%' // optional
		});

		$('input[name="donationAmt"]').on('ifChecked', function(event){
			if($(this).val() == '-1')
			{
				$('#otherAmount').attr('disabled', false);
			}
			else
			{
				$('#otherAmount').val('');
				$('#otherAmount').attr('disabled', true);
				if(Number($(this).val()) < 50)
				{
					$('#size').attr('disabled', true);
					$('#size').val('-1');
				}
				else
				{
					$('#size').attr('disabled', false);
				}
			}
		}); 	
		
		$('input[name="donationAmtType"]').iCheck({
		    checkboxClass: 'icheckbox_square-blue',
		    radioClass: 'iradio_square-blue',
		    increaseArea: '20%' // optional
		});

		$('input[name="donationAmtType"]').on('ifChecked', function(event){
			if($(this).val() == '1')
			{
				$('#donationAmt').val("-1");
				$('#otherAmount').attr('disabled', false);
				$('#otherAmount').rules("add", {
					required: true,
					digits: true
				});
			}
			else
			{
				$('#donationAmt').val("${lp}");
				$('#otherAmount').val('');
				$('#otherAmount').attr('disabled', true);
				$('#otherAmount').rules("remove");
				$('#donationAmtTypeDiv').removeClass('has-error');
				$('#donationAmtTypeDiv .help-block').remove();
			}
		}); 

		$('#otherAmount').change(function () {
			if(isNaN(Number($(this).val())))
			{
				alert("Digits allowed only.")
				return;
			}
			if(Number($(this).val()) < 50)
			{
				$('#size').attr('disabled', true);
				$('#size').val('-1');
			}
			else
			{
				$('#size').attr('disabled', false);
			}
		}); 
	
		$("#jq-validation-form")[0].reset();
		
		$('#inlineCheckbox1, #checkbox').iCheck({
			    checkboxClass: 'icheckbox_square-blue',
			    radioClass: 'iradio_square-blue',
			    increaseArea: '20%' // optional
		});
		  
		$("#country").select2({
		    allowClear: true
		});

		$('#country').val("United States").trigger("change");
		
		// Setup validation
		var validationRulesForCredit = {
				'firstName': {
					required: true
				},
				'lastName': {
					required: true
				},
				'address': {
					required: true
				},
				'city': {
					required: true
				},
				'state': {
					required: true
				},
				'zip': {
					required: true
				},
				'country': {
					required: true
				},
				"email": {
						required: true,
						email: true
				},
				"cardNumber": {
					required: true,
					creditcard: true
				},
				"expirationDateMonth": {
					required: true
				},
				"expirationDateYear": {
					required: true
				},
				"cardCode": {
					required: true,
					digits: true,
					rangelength: [3, 3]
				}
			};
		$("#jq-validation-form").validate({
			ignore: '.ignore, .select2-input',
			focusInvalid: true,
			rules: validationRulesForCredit,
			messages: {
				'jq-validation-policy': 'You must check it!'
			}
		});
		
		$('#submitButton').click(function(){
			$("#jq-validation-form").submit();
		});
		
	});
	window.PixelAdmin.start(init);
</script>
</body>
</html>
