<%@page import="java.util.List"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil.SelectOption"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%> 
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>CCHR International The Mental Health Watchdog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/widgets.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/themes.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/stylesheets/select2.min.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/assets/icheck/skins/square/blue.css" rel="stylesheet">

	<style>
		body{
			font-family : "Trebuchet MS", "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size: 13pt;
		}
		.theme-default{
			background-color: #fff;
		}
		.row {
			margin-left: 0;
			margin-right: 0;
		}
		.progress {
			height: 3px;
		}
		.headline {
			font-size : 23pt;
		}
		.topheaderline {
			font-family : "Times New Roman", "Trebuchet MS", "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size : 37pt;
			color: #000;
		}
		.sectionhead {
			font-size : 20pt;
		}
		
		.desc {
			font-size : 15pt;
		}
		.card-div{
			float:left;
			margin-right:15px;
		}
		.credit-card-black
		{
			background:url(${pageContext.request.contextPath}/assets/images/cchr/creditCardType-black.png);
			background-repeat:no-repeat;
			cursor:pointer;
		}
		
		.credit-card
		{
			background:url(${pageContext.request.contextPath}/assets/images/cchr/creditCardType.png);
			background-repeat:no-repeat;
			cursor:pointer;
		}
		
		.master
		{
			background-position:0px 0px;
			height:60px;
			width:95px;
		}
		
		.visa
		{
			background-position:-95px 0px;
			height:60px;
			width:95px;
		}
		
		.american-express
		{
			background-position:-190px 0px;
			height:60px;
			width:95px;
		}
		
		.discover
		{
			background-position:-285px 0px;
			height:60px;
			width:95px;
		}
	</style>
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/assets/images/cchr/logo.png">
	<!--[if lt IE 9]>
		<script src="${pageContext.request.contextPath}/assets/javascripts/ie.min.js"></script>
	<![endif]-->
</head>
<body class="theme-default main-menu-animated">

<script>var init = [];</script>

<tiles:insertAttribute name="header" />

<form class="form-horizontal" action="${pageContext.request.contextPath}/donate" method="post" modelAttribute="donationRecord" id="jq-validation-form">
	<div class="row" style=";font-weight: bold">
		<div class="container marketing" style="color:#000;">
		
			<tiles:insertAttribute name="donationAmt" />
			
			<tiles:insertAttribute name="donationGift" />
			
			<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>
			<div class="row" style=";font-weight: bold">
				<div class="col-sm-12">
					<span class="sectionhead">Billing Address:</span>
				</div>
			</div>
			<div class="row" style="margin-top:25px">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">First Name:<font color="red">*</font></label>
					<div class="col-sm-5">
						<input type="text" placeholder="First Name" name="firstName" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">Last Name:<font color="red">*</font></label>
					<div class="col-sm-5">
						<input type="text" placeholder="Last Name" name="lastName" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">Address:<font color="red">*</font></label>
					<div class="col-sm-5">
						<input type="text" placeholder="Address" name="address" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-5">
						<input type="text" placeholder="Address" name="address2" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">City:<font color="red">*</font></label>
					<div class="col-sm-5">
						<input type="text" placeholder="City" name="city" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">State/Province:<font color="red">*</font></label>
					<div class="col-sm-5">
						<input type="text" placeholder="State/Province" name="state" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">Zip/Postal Code:<font color="red">*</font></label>
					<div class="col-sm-5">
						<input type="text" placeholder="Zip/Postal Code" name="zip" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">Country:<font color="red">*</font></label>
					<div class="col-sm-5">
						<%= HtmlComponentUtil.getSelect("country", "country", (List<SelectOption>)request.getAttribute("countryDatas"), null, "Country") %>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">Phone:</label>
					<div class="col-sm-5">
						<input type="text" placeholder="Phone" name="phone" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label">Email:<font color="red">*</font></label>
					<div class="col-sm-5">
						<input type="text" placeholder="Email" id="email" name="email" class="form-control">
					</div>
				</div>
			</div>
			<br>
			<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>
			<div class="row" style=";font-weight: bold">
				<div class="col-sm-12">
					<span class="sectionhead">Payment Information:</span>
				</div>
			</div>
			<div id="creditCardInfo" style="margin-top:25px">
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-sm-2 control-label">&nbsp;</label>
						<div class="col-sm-10">
							<img src="${pageContext.request.contextPath}/assets/images/cchr/creditCardType.png" style="max-width:190x;max-height:30px">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-sm-2 control-label">Card Type:<font color="red">*</font></label>
						<div class="col-sm-5">
							<select id="cardType" name="cardType" class="unRender">
								<option value="mc">MasterCard</option>
								<option value="vs">VISA</option>
								<option value="ae">American Express</option>
								<option value="dn">Discover</option>
							<select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-sm-2 control-label">Card Number:<font color="red">*</font></label>
						<div class="col-sm-5">
							<input type="text" placeholder="9999 9999 9999 9999" name="cardNumber" id="cardNumber" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-sm-2 control-label">Expiration:<font color="red">*</font></label>
						<div class="col-sm-5">
							<%= HtmlComponentUtil.getSelect("expirationDateMonth", "expirationDateMonth", (List<SelectOption>)request.getAttribute("MONTH"), null, "MONTH", "width: 120px") %>
							/
							<%= HtmlComponentUtil.getSelect("expirationDateYear", "expirationDateYear", (List<SelectOption>)request.getAttribute("YEAR"), null, "YEAR", "width: 120px") %>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-sm-2 control-label">Security ID:<font color="red">*</font></label>
						<div class="col-sm-5">
							<input type="text" name="cardCode" id="cardCode" style="width:120px">
							<font color="#BFBFBF">Security code on back of card.</font>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group no-margin-hr">
					<label class="col-sm-2 control-label"><input type="checkbox" id="usePaypal" name="usePaypal" /></label>
					<div class="col-sm-5">
						<a onclick="javascript:window.open('https://www.paypal.com/al/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');" id="_ctl0_ContentPlaceHolder1_whatispaypal" href="javascript:void(0)">
							<img width="236" height="60" src="${pageContext.request.contextPath}/assets/images/cchr/paypal-logo.png">
						</a>
					</div>
				</div>
			</div>
			<br>
			<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>
			<div class="row">
				<div class="col-sm-4">
				</div>
				<div class="col-sm-4">
					<img width=100% height="100%" id="submitButton" style="cursor:pointer;" src="${pageContext.request.contextPath}/assets/images/cchr/<tiles:getAsString name="placeyourorder" />">
				</div>
				<div class="col-sm-4">
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="effort" value="${effort }">
 	<input type="hidden" name="mailingID" value="${mailingID }">
	<input type="hidden" name="reportID" value="${reportID }"> 
</form>

<div class="row" style="margin-top:30px;margin-bottom:30px">
	<div class="container marketing" style="height:50px">

	  <!-- Three columns of text below the carousel -->
	  <div class="row" style="text-align:center;color:#000;font-size:14px">
		<p>&copy 2015 Citizens Commission on Human Rights International. All Rights Reserved.</p>
<p>6616 Sunset Blvd., Los Angeles, CA 90028</p>
<p>323-467-4242 / 800-869-2247</p>
		
	  </div>
</div>

<div class="row" style="margin-top:30px;margin-bottom:30px">
	
</div>
<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="${pageContext.request.contextPath}/assets/javascripts/jquery-2.0.3.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="${pageContext.request.contextPath}/assets/javascripts/jquery-1.8.3.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="${pageContext.request.contextPath}/assets/javascripts/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/javascripts/pixel-admin.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/javascripts/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/icheck/icheck.min.js"></script>

<script type="text/javascript">
	init.push(function () {
	
		// 充值界面
		$("#jq-validation-form")[0].reset();
		
		 $('#inlineCheckbox1, #checkbox').iCheck({
			    checkboxClass: 'icheckbox_square-blue',
			    radioClass: 'iradio_square-blue',
			    increaseArea: '20%' // optional
			  });
		  
		$("select:not(.unRender)").select2({
		    allowClear: true
		});

		$('#country').val("United States").trigger("change");
		
		// Javascript code here
		$('input[name="donationAmtType"]').click(function () {
			if($(this).val() == '1')
			{
				$('#donationAmt').val("-1");
				$('#otherAmount').attr('disabled', false);
				$('#otherAmount').focus();
				$('#otherAmount').rules("add", {
					required: true,
					digits: true
				});
			}
			else
			{
				$('#donationAmt').val("${lp}");
				$('#otherAmount').val('');
				$('#otherAmount').attr('disabled', true);
				$('#otherAmount').rules("remove");
				$('#donationAmtTypeDiv').removeClass('has-error');
				$('#donationAmtTypeDiv .help-block').remove();
			}
		}); 

		$('#otherAmount').change(function () {
			if(isNaN(Number($(this).val())))
			{
				alert("Digits allowed only.")
				return;
			}
			if(Number($(this).val()) < 50)
			{
				$('#size').attr('disabled', true);
				$('#size').val('-1');
			}
			else
			{
				$('#size').attr('disabled', false);
			}
		}); 
		
		// Setup validation
		var validationRulesForCredit = {
				'firstName': {
					required: true
				},
				'lastName': {
					required: true
				},
				'address': {
					required: true
				},
				'city': {
					required: true
				},
				'state': {
					required: true
				},
				'zip': {
					required: true
				},
				'country': {
					required: true
				},
				"email": {
						required: true,
						email: true
				},
				"cardNumber": {
					required: true,
					creditcard: true
				},
				"expirationDateMonth": {
					required: true
				},
				"expirationDateYear": {
					required: true
				},
				"cardCode": {
					required: true,
					digits: true,
					rangelength: [3, 3]
				}
			};
		$("#jq-validation-form").validate({
			ignore: '.ignore, .select2-input',
			focusInvalid: true,
			rules: validationRulesForCredit,
			messages: {
				'jq-validation-policy': 'You must check it!'
			}
		});
		
		$('#submitButton').click(function(){
			$("#jq-validation-form").submit();
		});
		
		$('#usePaypal').click(function(){
			if(this.checked)
			{
				$('#creditCardInfo').hide();
				$('#cardNumber').rules("remove");
				$('#expirationDateMonth').rules("remove");
				$('#expirationDateYear').rules("remove");
				$('#cardCode').rules("remove");
			}
			else
			{
				$('#creditCardInfo').show();
				$('#cardNumber').rules("add", {
					required: true,
					creditcard: true
				});
				$('#expirationDateMonth').rules("add", {
					required: true
				});
				$('#expirationDateYear').rules("add", {
					required: true
				});
				$('#cardCode').rules("add", {
					required: true,
					digits: true,
					rangelength: [3, 3]
				});
			}
		});
		
		$('#cardType').change(function(){
			$('#cardNumber').unmask();
			if($(this).val() == 'ae')
			{
				$('#cardNumber').mask("9999 999999 99999");
				$('#cardNumber').attr('placeholder', '9999 999999 99999');
			}
			else
			{
				$('#cardNumber').mask("9999 9999 9999 9999");
				$('#cardNumber').attr('placeholder', '9999 9999 9999 9999');
			}
		});
		
		$('#cardNumber').mask("9999 9999 9999 9999");
	});
	window.PixelAdmin.start(init);
</script>
<!--  add GA code
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69790613-1', 'auto');
  ga('send', 'pageview');
</script>
-->
</body>
</html>
