<div class="row">
	<div class="col-sm-12">
		<div class="form-group no-margin-hr">
			<label class="checkbox-inline">
				<input type="checkbox" id="inlineCheckbox1" value="MONTHLY_DONATION" name="donationType" class="px"> 
				<span class="desc" style="font-weight: bold">Make this a monthly donation.</span>
				<br>
				<span class="desc" style="padding-left:30px"><font color="#3D3D3D" style="font-style: italic">CCHR will automatically receive your gift each month.</font></span>
			</label>			
		</div>
	</div>
</div>
<br>
<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>