<%@page import="java.util.List"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil.SelectOption"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil"%>
<div class="row" style=";font-weight: bold">
	<div class="col-sm-12">
		<span class="sectionhead">Billing Address:</span>
	</div>
</div>
<div class="row" style="margin-top:25px">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">First Name:<font color="red">*</font></label>
		<div class="col-sm-5">
			<input type="text" placeholder="First Name" name="firstName" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">Last Name:<font color="red">*</font></label>
		<div class="col-sm-5">
			<input type="text" placeholder="Last Name" name="lastName" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">Address:<font color="red">*</font></label>
		<div class="col-sm-5">
			<input type="text" placeholder="Address" name="address" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-5">
			<input type="text" placeholder="Address" name="address2" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">City:<font color="red">*</font></label>
		<div class="col-sm-5">
			<input type="text" placeholder="City" name="city" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">State/Province:<font color="red">*</font></label>
		<div class="col-sm-5">
			<input type="text" placeholder="State/Province" name="state" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">Zip/Postal Code:<font color="red">*</font></label>
		<div class="col-sm-5">
			<input type="text" placeholder="Zip/Postal Code" name="zip" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">Country:<font color="red">*</font></label>
		<div class="col-sm-5">
			<%= HtmlComponentUtil.getSelect("country", "country", (List<SelectOption>)request.getAttribute("countryDatas"), null, "Country") %>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">Phone:</label>
		<div class="col-sm-5">
			<input type="text" placeholder="Phone" name="phone" class="form-control">
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label">Email:<font color="red">*</font></label>
		<div class="col-sm-5">
			<input type="text" placeholder="Email" id="email" name="email" class="form-control">
		</div>
	</div>
</div>
<br>
<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>
