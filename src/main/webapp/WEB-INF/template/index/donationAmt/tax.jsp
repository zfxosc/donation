<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%> 
<div class="progress" style=""><div style="width: 100%;" class="progress-bar"></div></div>
<div class="row">
	<div class="col-sm-12">
		<span class="sectionhead">Select your donation amount:</span>
	</div>
</div>
<div class="row" style="margin-top:25px;">
	<div class="col-sm-12">
		<input type="radio" checked="checked" name="donationAmtType" value="0"> <span class="sectionhead checkbox-inline donationAmtType" style="margin-top:-15px">I am matching my ${param.py } gift of $${lps}</span>
		<input type="hidden" id="donationAmt" name="donationAmt" value="${lp}">
	</div>
</div>
<div class="row" style="margin-top:25px;">
	<div id="donationAmtTypeDiv" class="col-sm-12 form-group no-margin-hr">
		<input type="radio" name="donationAmtType" value="1"> <span class="sectionhead checkbox-inline" style="margin-top:-15px">Your Preferred Amount: $</span> <input type="text" name="otherAmount" id="otherAmount" disabled>
	</div>
</div>
<br>
<br>
<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>