<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil.SelectOption"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil"%>
<div class="row">
	<div class="col-sm-12">
		<span class="sectionhead">Select your donation amount:</span>
	</div>
</div>
<div class="row" style="margin-top:25px;">
	<%
		List<String> donateAmounts = (List<String>)request.getAttribute("donateAmount");
		for(int i=0; i<donateAmounts.size(); i++)
		{
			String donateAmount = donateAmounts.get(i);
			String checked = "";
			if(false)
			{
				checked = "checked=\"checked\"";
			}
	%>
		<div class="col-lg-2 col-md-2 col-sm-2">
			<input type="radio" name="donationAmt" <%= checked%> value="<%= donateAmount%>"> <span class="sectionhead checkbox-inline" style="margin-top:-15px">$<%= donateAmount%></span>
		</div>
	<%
		}
	%>
	<div class="col-lg-2 col-md-2 col-sm-2">
			<input type="radio" name="donationAmt" value="-1">   <span class="sectionhead checkbox-inline" style="margin-top:-15px">Other</span>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4">
		<input type="text" name="otherAmount" id="otherAmount" disabled >
	</div>
</div>
