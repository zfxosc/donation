<%
	Integer exAmount = (Integer)request.getAttribute("exAmount");
	if(exAmount < 1000)
	{
%>
<div class="row" style="margin-top: 40px; margin-bottom:25px">
	<div class="col-sm-12">
		<table width="100%" height="100%">
			<tr>
				<td width="70%">
					<span class="desc">Give a gift of $50 or more and receive a free "STOP Psychiatric Drugging of Kids" T-shirt - a $25 retail value. <font size="3px" color="#3D3D3D" style="font-style: italic">(Shipped to the billing address you enter below.)</font></span>
				</td>
				<td rowspan="2">
					<img width="100%" height="100%" src="${pageContext.request.contextPath}/assets/images/cchr/guy-tshirt.png" style="max-width:180px;max-height:180px">
				</td>
			</tr>
			<tr>
				<td>
					<br>
					<span class="sectionhead checkbox-inline" style="margin-top:-15px;margin-left:25px;margin-right:15px">Size:</span>
						<select id="size" name="tshirtSize" class="unRender" disabled>
									<option value="-1"></option>
									<option value="Small">Small(Unisex)</option>
									<option value="Medium">Medium(Unisex)</option>
									<option value="Large">Large(Unisex)</option>
									<option value="X-Large">X-Large(Unisex)</option>
								<select>
						
					</td>
				</tr>
			</table>
		</div>
	</div>
<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>
<%
	}
%>