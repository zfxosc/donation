<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<br>
<br>
<div class="row">
	<div class="col-sm-12">
		<table width="100%" height="100%">
			<tr>
				<td width="70%">
					<span class="desc">Give $25 or more and enjoy a free membership:</span>
					<span class="desc">
						<ul>
							<li>Free membership card</li>
							<li>Subscription to monthly newsletter</li>
							<li>“Psychiatric Drugs and Your Child’s Future”</li>
						</ul>
					</span>
					<span>
						<font size="3px" color="#3D3D3D" style="font-style: italic">(Shipped to the billing address you enter below.)</font>
					</span>
				</td>
				<td rowspan="2">
					<img width="100%" height="100%" src="${pageContext.request.contextPath}/assets/images/cchr/membership.jpg" style="max-width:456px;max-height:201px">
				</td>
			</tr>
		</table>
	</div>
</div>
<br>
<br>
<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>