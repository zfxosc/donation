<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="row" style="margin-top: 40px; margin-bottom:25px">
	<div class="col-sm-12">
		<table width="100%" height="100%">
			<tbody><tr>
				<td width="70%">
					<span class="desc">Give a gift of $25 or more and receive a free booklet: “Psychiatric Drugs and Your Child’s Future.” <font size="3px" color="#3D3D3D" style="font-style: italic">(Shipped to the billing address you enter below.)</font></span>
				</td>
				<td rowspan="2">
					<img width="100%" height="100%" style="max-width:133px;max-height:200px" src="${pageContext.request.contextPath}/assets/images/cchr/book-icon.png">
				</td>
			</tr>
		</tbody></table>
	</div>
</div>
<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>