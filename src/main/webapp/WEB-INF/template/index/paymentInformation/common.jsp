<%@page import="java.util.List"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil.SelectOption"%>
<%@page import="com.cchr.donation.util.HtmlComponentUtil"%>
<div class="row" style=";font-weight: bold">
	<div class="col-sm-12">
		<span class="sectionhead">Payment Information:</span>
	</div>
</div>
<div id="creditCardInfo" style="margin-top:25px">
	<div class="row">
		<div class="form-group no-margin-hr">
			<label class="col-sm-2 control-label">&nbsp;</label>
			<div class="col-sm-10">
				<img src="${pageContext.request.contextPath}/assets/images/cchr/creditCardType.png" style="max-width:190x;max-height:30px">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group no-margin-hr">
			<label class="col-sm-2 control-label">Card Type:<font color="red">*</font></label>
			<div class="col-sm-5">
				<select id="cardType" name="cardType" class="unRender">
					<option value="mc">MasterCard</option>
					<option value="vs">VISA</option>
					<option value="ae">American Express</option>
					<option value="dn">Discover</option>
				<select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group no-margin-hr">
			<label class="col-sm-2 control-label">Card Number:<font color="red">*</font></label>
			<div class="col-sm-5">
				<input type="text" placeholder="9999 9999 9999 9999" name="cardNumber" id="cardNumber" class="form-control">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group no-margin-hr">
			<label class="col-sm-2 control-label">Expiration:<font color="red">*</font></label>
			<div class="col-sm-5">
				<%= HtmlComponentUtil.getSelect("expirationDateMonth", "expirationDateMonth", (List<SelectOption>)request.getAttribute("MONTH"), null, "MONTH", "width: 120px") %>
				/
				<%= HtmlComponentUtil.getSelect("expirationDateYear", "expirationDateYear", (List<SelectOption>)request.getAttribute("YEAR"), null, "YEAR", "width: 120px") %>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group no-margin-hr">
			<label class="col-sm-2 control-label">Security ID:<font color="red">*</font></label>
			<div class="col-sm-5">
				<input type="text" name="cardCode" id="cardCode" style="width:120px">
				<font color="#BFBFBF">Security code on back of card.</font>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group no-margin-hr">
		<label class="col-sm-2 control-label"><input type="checkbox" id="usePaypal" name="usePaypal" /></label>
		<div class="col-sm-5">
			<a onclick="javascript:window.open('https://www.paypal.com/al/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');" id="_ctl0_ContentPlaceHolder1_whatispaypal" href="javascript:void(0)">
				<img width="236" height="60" src="${pageContext.request.contextPath}/assets/images/cchr/paypal-logo.png">
			</a>
		</div>
	</div>
</div>
<br>
<div class="progress"><div style="width: 100%;" class="progress-bar"></div></div>
<script type="text/javascript">
	init.push(function () {
		$('#usePaypal').click(function(){
			if(this.checked)
			{
				$('#creditCardInfo').hide();
				$('#cardNumber').rules("remove");
				$('#expirationDateMonth').rules("remove");
				$('#expirationDateYear').rules("remove");
				$('#cardCode').rules("remove");
			}
			else
			{
				$('#creditCardInfo').show();
				$('#cardNumber').rules("add", {
					required: true,
					creditcard: true
				});
				$('#expirationDateMonth').rules("add", {
					required: true
				});
				$('#expirationDateYear').rules("add", {
					required: true
				});
				$('#cardCode').rules("add", {
					required: true,
					digits: true,
					rangelength: [3, 3]
				});
			}
		});
		
		$('#cardType').change(function(){
			$('#cardNumber').unmask();
			if($(this).val() == 'ae')
			{
				$('#cardNumber').mask("9999 999999 99999");
				$('#cardNumber').attr('placeholder', '9999 999999 99999');
			}
			else
			{
				$('#cardNumber').mask("9999 9999 9999 9999");
				$('#cardNumber').attr('placeholder', '9999 9999 9999 9999');
			}
		});
		
		$('#cardNumber').mask("9999 9999 9999 9999");
	});
</script>