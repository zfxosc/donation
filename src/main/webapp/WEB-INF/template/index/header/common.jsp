<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%> 
<div class="row">
	<div class="col-sm-12" style="text-align:center">
		  	<img width="100%" height="100%" style="max-width:802px;max-height:184px" src="${pageContext.request.contextPath}/assets/images/cchr/logo.jpg">
		</div>
</div>

<div class="row" style="height: 10px;background-color: #000;">
	<div class="progress" style="width: 100%; height: 10px"></div>
</div>

<tiles:insertAttribute name="headerBanner" />

<div class="row">
	<tiles:insertAttribute name="topHeaderDesc" />
</div>