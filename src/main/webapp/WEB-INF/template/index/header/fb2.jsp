<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="row">
	<div class="col-sm-12" style="text-align:center">
		  	<img width="100%" height="100%" style="max-width:802px;max-height:184px" src="${pageContext.request.contextPath}/assets/images/cchr/logo.jpg">
		</div>
</div>
<div class="row" style="height: 10px;background-color: #000;">
	<div class="progress" style="width: 100%; height: 10px"></div>
</div>
<div class="row headline" style="background-color:#bdbec1;">
	<div class="col-sm-12">
	</div>
	<div class="col-sm-1">
	</div>
	<div class="col-sm-5" style="color:#000; font-weight: bold; padding-top: 0px; text-align:right">
		<img width="100%" height="395px" style="max-width:652px;max-height:395px" src="${pageContext.request.contextPath}/assets/images/cchr/support2.jpg">
	</div>
	<div class="col-sm-5">
		<iframe height=330px width=100% src="https://www.youtube.com/embed/gywCn1T-Jug?autoplay=1" frameborder=0 allowfullscreen style="margin-top:30px"></iframe>
	</div>
	<div class="col-sm-1">
	</div>
</div>

<div class="row">
	<div class="container marketing" style="color:#000; margin-top:50px;">
		<div class="row" >
			<p>
				<img src="${pageContext.request.contextPath}/assets/images/cchr/0-5-year-olds-psych-drugs.jpg" class="pull-left" width="100%" height="100%" style="max-height:235px;max-width:400px;margin-right:20px">
			</p>
			<p class="desc" style="margin-top: -10px">
				Psychiatric drugging is BIG business... An $84 billion-a-year industry.  
				<br>
				<br>
					But what's the fastest growing market for this multi-million dollar industry? Believe it or not, it's children between the ages of zero to one. Newborns. Babies. Toddlers.
					<br>
				<br>
			</p>
		</div>
		
		<div class="row">
			<div class="col-sm-12">
				<p class="desc" style="margin-top: 20px">
					<strong>There are over a million children ages 5 and under on psychiatric drugs:</strong> 
				</p>
			</div>
		</div>
		<div class="row desc" style="text-align:left">
			<div class="col-sm-2 col-sm-offset-4">
				0-1 Year Olds
			</div>
			<div class="col-sm-2">
				274,804                    
			</div>
		</div>
		<div class="row desc" style="text-align:left">
			<div class="col-sm-2 col-sm-offset-4">
				2-3 Year Olds
			</div>
			<div class="col-sm-2">
				370,778                  
			</div>
		</div>
		<div class="row desc" style="text-align:left">
			<div class="col-sm-2 col-sm-offset-4">
				4-5 Year Olds
			</div>
			<div class="col-sm-2">
				500,948                  
			</div>
		</div>
		<div class="row desc" style="text-align:left">
			<div class="col-sm-2 col-sm-offset-4">
				<strong>Total</strong>
			</div>
			<div class="col-sm-2">
				<strong>1,080,168</strong>               
			</div>
		</div>
		<div class="row" style="margin-top: 20px">
			<p class="desc">
				The first step in ending atrocities is bringing the facts to light. And only the Citizens Commission on Human Rights (CCHR) is obtaining and exposing these statistics and all of the documented risks of these drugs.
			</p>
			<p class="desc">
				Our vital work is prompting articles in The Wall Street Journal, Psychology Today, and hundreds of other print and online sources â all citing CCHRâs hard-won facts such as these:
			</p>
		</div>
		<div class="row desc" style="text-align:left">
			<div class="col-sm-11 col-sm-offset-1">
				<strong>
					<p class="desc" style="margin-top: 20px">
						1.&nbsp;&nbsp;&nbsp;&nbsp;Babies and toddlers are being given drugs so powerful the U.S. Drug Enforcement Administration puts them in the same category of highly addictive drugs as heroin, cocaine and opium.
					</p>
					<p class="desc" style="margin-top: 20px">
						2.&nbsp;&nbsp;&nbsp;&nbsp;Children up to kindergarten age are prescribed drugs so dangerous they cause diabetes, heart attack, suicide, violence, psychotic reactions and sudden death according to the U.S. Food and Drug Administration.
					</p>
					<p class="desc" style="margin-top: 20px">
						3.&nbsp;&nbsp;&nbsp;&nbsp;The psychiatric/pharmaceutical industry has made it 100% clear it cares for profit far above the safety and well-being of our children.
					</p>
				</strong>
			</div>
		</div>
		<div class="row" style="margin-top: 20px">
			<p class="desc">
				The facts are indisputable. The only question remaining is... Who will fight for our kids? 
			</p>
			<p class="desc">
				We ask you to stand shoulder to shoulder with CCHR, as we battle on behalf of defenseless children who are needlessly drugged.
			</p>
			<p class="desc">
				Our kids deserve better than a life of psychiatric drugs. Parents deserve our help. And concerned citizens, like you, deserve to know the truth. Join with us... 
			</p>
			<p class="desc">
				 CCHR fights every day on behalf of the 1,000,000+ children ages 5 and under who are on harmful psychiatric drugs.
			</p>
			<p class="desc">
				Your contribution today helps CCHR continue to provide help to at-risk kids and their parents. Thank you!
			</p>
		</div>
		
		<div class="progress" style="margin-top:40px;"><div style="width: 100%;" class="progress-bar"></div></div>
	</div>
</div>