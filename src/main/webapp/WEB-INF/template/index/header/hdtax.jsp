<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
<!--
.theme-default .progress-bar{
	background:#2431AB none repeat scroll 0 0;
	border-color:#2431AB;
}
-->
</style>
<div class="row headline" style="background-color:#4297CE;">
	<img width="100%" height="100%" src="${pageContext.request.contextPath}/assets/images/index-hd/index-hd-header-blue.jpg">
</div>

<div class="row">
	<div class="col-sm-2">
	</div>
	<div class="col-sm-8">
		<img width="100%" height="100%" src="${pageContext.request.contextPath}/assets/images/index-hd/hatax.jpg">
	</div>
	<div class="col-sm-2">
	</div>
</div>

<div class="row">
	<div class="container marketing" style="color:#000;">
		<div class="row" style="color:#000;margin-top:50px;">
			<div class="col-sm-12">
				<p class="desc" style="margin-top: -10px">
					Every day, millions of children suffer abuse under the guise of psychiatric "help."
					<br>
					<br>
						As a Citizens Commission on Human Rights (CCHR) Elite Stateholder, your gift helps us
fight on behalf of these at-risk kids... and meet our urgent 2015 end-of-year goals.
						<br>
					<br>
				</p>
			</div>
		</div>
	</div>
</div>