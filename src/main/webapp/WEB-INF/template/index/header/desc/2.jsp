<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container marketing" style="color:#000;">
	<div class="desc" style="margin-top:35px;">Citizens Commission on Human Rights (CCHR) fights every day on behalf of the 1,000,000+ children ages 5 and under who are on harmful psychiatric drugs.</div>
	<div class="desc" style="margin-top:20px;">Your contribution today helps CCHR continue to provide help to at-risk kids and their parents. Thank you!</div>
	<div class="progress" style="margin-top:40px;"><div style="width: 100%;" class="progress-bar"></div></div>
</div>
