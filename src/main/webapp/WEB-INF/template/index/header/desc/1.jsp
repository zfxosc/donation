<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container marketing" style="color:#000;">
	<div class="desc" style="margin-top:35px;">Citizens Commission on Human Rights (CCHR) has helped enact more than 150 laws protecting children and adults from abusive mental health practices.</div>
	<div class="desc" style="margin-top:20px;">Your contribution helps CCHR continue our campaign to fight for kids against the profit-driven psycho-pharmaceutical industry.</div>
	<div class="progress" style="margin-top:40px;"><div style="width: 100%;" class="progress-bar"></div></div>
</div>