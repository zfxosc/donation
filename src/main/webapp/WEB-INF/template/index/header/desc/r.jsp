<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container marketing" style="color:#000;">
		<div class="desc" style="margin-top:35px;">Together you and Citizens Commission on Human Rights (CCHR) can help stop avoidable tragedies like the death of Matthew Steubing.</div>
		<div class="desc" style="margin-top:20px;">Plus, make your contribution to CCHR today and deduct it from the 2015 tax year. It only takes a moment... and it will make a world of difference!</div>
		<div class="progress" style="margin-top:40px;"><div style="width: 100%;" class="progress-bar"></div></div>
	</div>