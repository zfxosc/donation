<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container marketing" style="color:#000;">
	<div class="desc" style="margin-top:35px;">Citizens Commission on Human Rights (CCHR) helps educate the community... Keep families together... And provide hope when no one else will.</div>
	<div class="desc" style="margin-top:20px;">Your contribution to CCHR goes a long way in loosening the psychiatric/pharmaceutical industry's grip on the health — and happiness — of our children.</div>
	<div class="progress" style="margin-top:40px;"><div style="width: 100%;" class="progress-bar"></div></div>
</div>