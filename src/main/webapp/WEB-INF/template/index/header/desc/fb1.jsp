<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="container marketing" style="color:#000; margin-top:50px;">
		<div class="row" >
			<p>
				<img src="${pageContext.request.contextPath}/assets/images/cchr/childhood-is-not-a-mental-disorder.jpg" class="pull-left" width="100%" height="100%" style="max-height:235px;max-width:400px;margin-right:20px">
			</p>
			<p class="desc" style="margin-top: -10px">
				You wouldn’t hesitate to dial 9-1-1 if you saw a child being physically threatened, beaten or abandoned. 
				<br>
				<br>
					Yet every day millions of children silently experience abuse at the hands of the psychiatric/pharmaceutical industry—being falsely diagnosed and dangerous drugs, with many facing institutionalization.
					<br>
				<br>
					Fortunately, Citizens Commission on Human Rights has been on the front lines of mental health reform since 1969... providing the antidote to the powers that continue to put profit before children's lives.
					<br>
				<br>
			</p>
		</div>
		<div class="row" >
			<p class="desc" style="margin-top: -10px">
				Please watch the short video above and arm yourself with 3 facts the psychiatric/pharmaceutical industry doesn't want you to know:
			</p>
			<div style="padding-left:70px">
				<p class="desc">
				 FACT #1: Over 8 million kids and adolescents in this country have been labeled as being "mentally ill." Yet there are no brain scans, x-rays, genetic or blood tests that can prove this diagnosis. What is certain is that the 8.3 million children, more than one million age five and under, are prescribed dangerous and potentially lethal drugs to ingest.
				 </p> 
				 <p class="desc" style="margin-top: 20px">
				 FACT #2: In the U.S. alone, 286 regulatory agency warnings cite dangerous, often fatal side effects of the psychiatric drugs commonly prescribed to children. These include diabetes... heart problems... strokes... mania... psychosis... hallucinations... seizures... and worse.
				 </p> 
				 <p class="desc" style="margin-top: 20px">
				 FACT #3: Psychiatric drugs CAN KILL. Forty-one drug regulatory warnings cite suicide as a side effect of the drugs prescribed to children. Twenty-three different warnings cite sudden death. When placed on anti-depressants and similar medications, children die. That is a fact.
				 </p> 
			</div>
		</div>
		<div class="row" >
			<p class="desc">
				Don't remain silent. Quickly learn what can be done to protect the millions of vulnerable kids who Citizens Commission on Human Rights advocates for each and every day.
			</p>
			<p class="desc">
				Citizens Commission on Human Rights (CCHR) has helped enact more than 150 laws protecting children and adults from abusive mental health practices.
			</p>
			<p class="desc">
				Your contribution today helps CCHR continue our fight against Big Pharma and its profit-driven lobbyists.
			</p>
		</div>
		<br>
		<div class="progress"><div class="progress-bar" style="width: 100%;"></div></div>
	</div>