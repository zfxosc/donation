<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
<!--
.theme-default .progress-bar{
	background:#A71931 none repeat scroll 0 0;
	border-color:#A71931;
}
.donationAmtType{
	color:#A71931;
}
-->
</style>
<div class="row headline" style="background-color:#4297CE;">
	<img width="100%" height="100%" src="${pageContext.request.contextPath}/assets/images/index-hd/index-hd-header-red.jpg">
</div>

<div class="row">
	<div class="col-sm-2">
	</div>
	<div class="col-sm-8">
		<img width="100%" height="100%" src="${pageContext.request.contextPath}/assets/images/index-hd/index-hd-banner-red.png">
	</div>
	<div class="col-sm-2">
	</div>
</div>

<div class="row">
	<div class="container marketing" style="color:#000;">
		<div class="row" style="color:#000;margin-top:50px;">
			<div class="col-sm-12">
				<p class="desc" style="margin-top: -10px">
					With a need this urgent, thank you in advance for going above and beyond by donating. 
					<br>
					<br>
						Citizens Commission on Human Rights (CCHR) simply could not provide the same level of life-savings services day after day - year after year - without the continued support of Elite Stakeholders, like you.
						<br>
					<br>
				</p>
			</div>
		</div>
	</div>
</div>