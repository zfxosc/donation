<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>CCHR International The Mental Health Watchdog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link href="<%=contextPath%>/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">

	<link rel="shortcut icon" href="<%=contextPath%>/assets/images/cchr/logo.png">
	<!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->
	<style>
		.row {
			margin-left: 0;
			margin-right: 0;
		}
	</style>
</head>
<body class="page-404">

<script>var init = [];</script>

<div class="row" style="background-color: #fff;">
	<div class="col-sm-12" style="text-align:center">
		  	<img width="100%" height="100%" style="max-width:802px;max-height:184px" src="<%=contextPath%>/assets/images/cchr/logo.jpg">
		</div>
</div>
	<div class="error-code">500</div>

	<div class="error-text">
		<span class="oops">OUCH!</span><br>
		<span class="hr"></span>
		<br>
		<%
			List<String> messages = (List<String>)request.getAttribute("message");
			if(messages == null || messages.size() == 0)
			{
		%>
				SOMETHING IS NOT QUITE RIGHT
				<br>
		<%
			}
			else
			{
				for(String message : messages)
				{
		%>
					<%=message %>
					<br>
		<%
				}
			}
		%>
		<span class="solve">We hope to solve it shortly</span>
	</div>


<div class="row" style="margin-top:30px;margin-bottom:30px;">
	<div class="container marketing" style="height:50px">

	  <!-- Three columns of text below the carousel -->
	  <div class="row" style="text-align:center;color:#000;font-size:14px">
		<p>&copy 2015 Citizens Commission on Human Rights International. All Rights Reserved.</p>
<p>6616 Sunset Blvd., Los Angeles, CA 90028</p>
<p>323-467-4242 / 800-869-2247</p>
		
	  </div>
</div>

<div class="row" style="margin-top:30px;margin-bottom:30px;">
	
</div>

</body>
</html>