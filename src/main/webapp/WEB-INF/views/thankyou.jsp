<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>CCHR International The Mental Health Watchdog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" href="<%=contextPath%>/assets/images/cchr/logo.png">
</head>
<body>
<div class="row headline" style="margin-top:5px; padding-top: 60px; padding-bottom: 30px;text-align:center">
	<div class="col-sm-4" style="text-align:center" >
		<img width="100%" height="100%" style="max-width:504px; max-height:567px" src="<%=contextPath%>/assets/images/cchr/thankyou.jpg">
	</div>
</div>

</body>
</html>