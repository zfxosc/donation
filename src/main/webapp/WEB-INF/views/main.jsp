<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>

<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Sign In</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Open Sans font from Google CDN -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

	<!-- Pixel Admin's stylesheets -->
	<link href="<%=contextPath%>/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/themes.min.css" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
		<script src="<%=contextPath%>/assets/javascripts/ie.min.js"></script>
	<![endif]-->

</head>


<!-- 1. $BODY ======================================================================================
	
	Body

	Classes:
	* 'theme-{THEME NAME}'
	* 'right-to-left'     - Sets text direction to right-to-left
-->
<body class="theme-default main-menu-animated">
<script>var init = [];</script>

<div id="main-wrapper">


<!-- 2. $MAIN_NAVIGATION ===========================================================================

	Main navigation
-->
	<div id="main-navbar" class="navbar navbar-inverse" role="navigation">
		<!-- Main menu toggle -->
		<button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">HIDE MENU</span></button>
		
		<div class="navbar-inner">
			<!-- Main navbar header -->
			<div class="navbar-header">

				<!-- Logo -->
				<a href="index.html" class="navbar-brand">
					<div><img alt="Pixel Admin" src="assets/images/pixel-admin/main-navbar-logo.png"></div>
					Abuse Case System
				</a>

				<!-- Main navbar toggle -->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse"><i class="navbar-icon fa fa-bars"></i></button>

			</div> <!-- / .navbar-header -->

			<div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
				<div>
					<ul class="nav navbar-nav">
						<li>
							<a href="index.html">Home</a>
						</li>
					</ul> <!-- / .navbar-nav -->

					<div class="right clearfix">
						<ul class="nav navbar-nav pull-right right-navbar-nav">

<!-- 3. $NAVBAR_ICON_BUTTONS =======================================================================

							Navbar Icon Buttons

							NOTE: .nav-icon-btn triggers a dropdown menu on desktop screens only. On small screens .nav-icon-btn acts like a hyperlink.

							Classes:
							* 'nav-icon-btn-info'
							* 'nav-icon-btn-success'
							* 'nav-icon-btn-warning'
							* 'nav-icon-btn-danger' 
-->
							
<!-- /3. $END_NAVBAR_ICON_BUTTONS -->


							<li class="dropdown">
								<a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
									<span>John Doe</span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#"><span class="label label-warning pull-right">New</span>Profile</a></li>
									<li><a href="abuse--resetpsd-alt.html"><i class="dropdown-icon fa fa-cog"></i>&nbsp;&nbsp;Reset Password</a></li>
									<li class="divider"></li>
									<li><a href="login.html"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
								</ul>
							</li>
						</ul> <!-- / .navbar-nav -->
					</div> <!-- / .right -->
				</div>
			</div> <!-- / #main-navbar-collapse -->
		</div> <!-- / .navbar-inner -->
	</div> <!-- / #main-navbar -->
<!-- /2. $END_MAIN_NAVIGATION -->


<!-- 4. $MAIN_MENU =================================================================================

		Main menu
		
		Notes:
		* to make the menu item active, add a class 'active' to the <li>
		  example: <li class="active">...</li>
		* multilevel submenu example:
			<li class="mm-dropdown">
			  <a href="#"><span class="mm-text">Submenu item text 1</span></a>
			  <ul>
				<li>...</li>
				<li class="mm-dropdown">
				  <a href="#"><span class="mm-text">Submenu item text 2</span></a>
				  <ul>
					<li>...</li>
					...
				  </ul>
				</li>
				...
			  </ul>
			</li>
-->
	<div id="main-menu" role="navigation">
		<div id="main-menu-inner">
			<div class="menu-content top" id="menu-content-demo">
				<!-- Menu custom content demo
					 CSS:        styles/pixel-admin-less/demo.less or styles/pixel-admin-scss/_demo.scss
					 Javascript: html/assets/demo/demo.js
				 -->
				<div>
					<div class="text-bg"><span class="text-slim">Welcome,</span> <span class="text-semibold">John</span></div>

					<img src="assets/demo/avatars/1.jpg" alt="" class="">
					<div class="btn-group">
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-envelope"></i></a>
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-user"></i></a>
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-cog"></i></a>
						<a href="#" class="btn btn-xs btn-danger btn-outline dark"><i class="fa fa-power-off"></i></a>
					</div>
					<a href="#" class="close">&times;</a>
				</div>
			</div>
			<ul class="navigation">
				<li>
					<a href="case-detail.html"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Create New Case</span></a>
				</li>
				<li>
					<a href="user-list.html"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">User Management</span></a>
				</li>
				<li class="active">
					<a href="index.html"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Case Management</span></a>
				</li>
			</ul> <!-- / .navigation -->
		</div> <!-- / #main-menu-inner -->
	</div> <!-- / #main-menu -->
<!-- /4. $MAIN_MENU -->

	<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">You are here: </div>
			<li><a href="#">Home</a></li>
			<li class="active"><a href="<%=contextPath%>/donate">ABUSE CASE LIST</a></li>
			<li><a href="<%=contextPath%>/donate">12344444</a></li>
		</ul>
		<div class="page-header">
			
			<div class="row">
				<!-- Page header, center on small screens -->
				<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-dashboard page-header-icon"></i>&nbsp;&nbsp;ABUSE CASE LIST</h1>

			</div>
		</div> <!-- / .page-header -->


		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					 <!--search conditions-->
					<div class="row">
						<div class="col-sm-12">
						<form class="form-horizontal " role="form" id="userList" method="post" action="#">
							<div class="panel-body" style="padding:20px">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label  class="col-sm-3 control-label">Patient Name:</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="patient-id" name="patient-name">
										</div> 
									</div>
								</div>						
								<div class="col-sm-4">
									<div class="form-group">
										<label  class="col-sm-3 control-label">Doctor Name:</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="doctor-id" name="doctor-name">
										</div> 
									</div>
								</div>					
								<div class="col-sm-4">
									<div class="form-group">
										<label  class="col-sm-3 control-label">Facility Name:</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" id="facility-id" name="facility-name">
										</div> 
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label  class="col-sm-3 control-label">CreateTime:</label>

										<div class="col-sm-7 input-daterange input-group" id="bs-datepicker-range">
											<input type="text" class="input-sm form-control" name="start" placeholder="Start date">
											<span class="input-group-addon">to</span>
											<input type="text" class="input-sm form-control" name="end" placeholder="End date">
										</div>
									</div>
								</div>
								<div class="col-sm-1">
									<button class="btn btn-primary">Search</button>
								</div>
								<div class="col-sm-2">
									<div class="checkbox">
										<label>
											<input type="checkbox" id="advancedcheckbox" name="advancedcheckbox" class="px"> 
											<span class="lbl">Advanced conditions</span>
										</label>
									</div>
								</div>
							</div>
							<div class="row" id="advanceArea" style="display:none;margin-top:30px">	
								<div class="col-sm-12">
									<div class="row">	
										<div class="col-sm-4">
											<div class="form-group">
												<label  class="col-sm-3 control-label">Country:</label>
												<div class="col-sm-7">
													<select class="form-control" id="jq-validation-select" name="jq-validation-select">
														<option value="">Select country...</option>
														<optgroup label="ASIA">
															<option value="china">CHIANA</option>
															<option value="cams">Cams</option>
															<option value="nuts">Nuts</option>
															<option value="bolts">Bolts</option>
															<option value="stoppers">Stoppers</option>
															<option value="sling">Sling</option>
														</optgroup>
														<optgroup label="Skiing">
															<option value="skis">Skis</option>
															<option value="skins">Skins</option>
															<option value="poles">Poles</option>
														</optgroup>
													</select>
												</div> 
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label  class="col-sm-3 control-label">State:</label>
												<div class="col-sm-7">
													<select class="form-control" id="jq-validation-select" name="jq-validation-select">
														<option>select state</option>
														<option value="AF">New York</option>
														<option value="AX">Alaska</option>
														<option value="AL">California</option>
													</select>
												</div> 
											</div>
										</div>	
										<div class="col-sm-4">
											<div class="form-group">
												<label  class="col-sm-3 control-label">city:</label>
												<div class="col-sm-7">
													<select class="form-control" name="signup_city" id="city_id">
														<option>select city</option>
														<option value="AF">New York</option>
														<option value="AX">Alaska</option>
														<option value="AL">California</option>
													</select>
												</div> 
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label  class="col-sm-3 control-label">Address:</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="address-id" name="address-name">
												</div> 
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label  class="col-sm-3 control-label">Disease:</label>
												<div class="col-sm-7">
													<select class="form-control" id="jq-validation-select" name="jq-validation-select">
														<option>select disease</option>
														<option value="AF">congestion</option>
														<option value="AX">stroke</option>
														<option value="AL">abnormal heart beat</option>
													</select>
												</div> 
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label  class="col-sm-3 control-label">Drugs:</label>
												<div class="col-sm-7">
													<select class="form-control" id="jq-validation-select" name="jq-validation-select">
														<option>select drugs</option>
														<option value="AF">aspirin</option>
														<option value="AX">aspirin</option>
														<option value="AL">aspirin</option>
													</select>
												</div> 
											</div>
										</div>										
									</div>
								</div>
							</div>

							</div>
						</form>
						</div>

					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel-body">
								<div class="table-primary">
									<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
										<thead>
											<tr>
												<th>PatientName</th>
												<th>DoctorName</th>
												<th>FelicityName</th>
												<th>CreateTime</th>
												<th>Operation</th>

											</tr>
										</thead>
										<tbody>
											<tr class="odd gradeX">
												<td>anna</td>
												<td>obama</td>
												<td>aaaa</td>
												<td class="center">2015-09-10</td>
												<td>
													<button class="btn btn-primary">Recall</button>
													<a class="btn btn-primary" href="case-detail.html">Update</a>
												</td>	
											</tr>
											<tr class="even gradeC">
												<td>anna</td>
												<td>obama</td>
												<td>aaaa</td>
												<td class="center">2015-09-10</td>
												<td>
													<button class="btn btn-primary">Recall</button>
													<a class="btn btn-primary" href="case-detail.html">Update</a>
												</td>												
											</tr>
											<tr class="odd gradeX">
												<td>anna</td>
												<td>obama</td>
												<td>aaaa</td>
												<td class="center">2015-09-10</td>
												<td>
													<button class="btn btn-primary">Recall</button>
													<a class="btn btn-primary" href="case-detail.html">Update</a>
												</td>
												
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-sm-offset-10">
									<ul class="pagination pagination-xs">
									<li><a href="#"><<</a></li>
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">...</a></li>
									<li><a href="#">>></a></li>	
									</ul> <!-- / .panel-heading-controls -->
								</div>
							</div>
						</div>
					</div>
				</div><!-- / #panel -->
	    	</div>
		</div><!-- / #row -->
	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->


<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<%=contextPath%>/assets/javascripts/jquery-2.0.3.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="<%=contextPath%>/assets/javascripts/jquery-1.8.3.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- Pixel Admin's javascripts -->
<script src="<%=contextPath%>/assets/javascripts/bootstrap.min.js"></script>
<script src="<%=contextPath%>/assets/javascripts/pixel-admin.min.js"></script>

</body>
</html>
