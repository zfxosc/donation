<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>CCHR International The Mental Health Watchdog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link href="<%=contextPath%>/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<%=contextPath%>/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">

	<style>
		body{
			font-family : "Trebuchet MS", "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size: 13pt;
		}
		.theme-default{
			background-color: #fff;
		}
		.row {
			margin-left: 0;
			margin-right: 0;
		}
		.progress {
			height: 3px;
		}
		.desc {
			font-family : "Times New Roman", "Trebuchet MS", "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size : 16pt;
			font-weight:normal;
		}
		.table-key{
			text-align:right;
		}
	</style>
	<link rel="shortcut icon" href="<%=contextPath%>/assets/images/cchr/logo.png">
</head>
<body class="theme-default main-menu-animated">

<script>var init = [];</script>

<div class="row">
	<div class="col-sm-12" style="text-align:center">
		  	<img width="100%" height="100%" style="max-width:802px;max-height:184px" src="<%=contextPath%>/assets/images/cchr/logo.jpg">
		</div>
</div>
<div class="row" style="height: 10px;background-color: #000;">
	<div class="progress" style="width: 100%; height: 10px"></div>
</div>
<div class="row headline" style="background-color:#4297CE;">
	<div class="col-sm-12" style="font-size: 90px;color:#fff; text-align:center;margin-top:50px;margin-bottom:50px">
		<strong>THANK YOU!</strong>
	</div>
</div>

<div class="row">
	<div class="container marketing" style="color:#000;">
		<div class="desc" style="margin-top:35px;">Dear {FIRST_NAME},</div>
		<div class="desc" style="margin-top:20px;">Thank you for your {DONATION_AMOUNT} donation to the <strong>Citizens Commission of Human Rights (CCHR).</strong></div>
		<div class="desc" style="margin-top:20px;">Since 1969, CCHR has been responsible for helping to enact more than 150 reforms protecting children and adults from abusive and coercive mental health practices. Your gift will not only advance more of these reforms, but will also educate families about the life-threatening dangers of psychotropic drugs.</div>
		<div class="desc" style="margin-top:20px;">I can't stress this enough: These ongoing successes are directly made possible thanks to contributions like yours. To learn more about <strong>CCHR</strong>, <a href="#">please visit our Welcome Page.</a></div>
		<div class="row" style="height: 10px;background-color: #000;margin-top:40px">
			<div class="progress" style="width: 100%; height: 10px"></div>
		</div>
		<div class="desc" style="margin-top:35px;font-size: 40px;text-align:center"><strong>Transaction Summary</strong></div>
		<div class="row desc" style="margin-top:20px;">
			<div class="form-group no-margin-hr">
				<label class="col-sm-6 control-label table-key desc">Transaction Date:</label>
				<div class="col-sm-6">
					{TRANSACTION_DATE}
				</div>
			</div>
		</div>
		<div class="row desc">
			<div class="form-group no-margin-hr">
				<label class="col-sm-6 control-label table-key desc">Donor's Name:</label>
				<div class="col-sm-6">
					{DONOR_NAME}
				</div>
			</div>
		</div>
		<div class="row desc">
			<div class="form-group no-margin-hr">
				<label class="col-sm-6 control-label table-key desc">Payment Type:</label>
				<div class="col-sm-6">
					{PAYMENT_TYPE}
				</div>
			</div>
		</div>
		<div class="row desc">
			<div class="form-group no-margin-hr">
				<label class="col-sm-6 control-label table-key desc">Credit Card No.:</label>
				<div class="col-sm-6">
					{CREDIT_CARD_NO}
				</div>
			</div>
		</div>
		<div class="row desc">
			<div class="form-group no-margin-hr">
				<label class="col-sm-6 control-label table-key desc">Total Gift Amount:</label>
				<div class="col-sm-6">
					{TOTAL_GIFT_AMOUNT}
				</div>
			</div>
		</div>
		<div class="row" style="height: 10px;background-color: #000;margin-top:40px">
			<div class="progress" style="width: 100%; height: 10px"></div>
		</div>
		<div class="desc" style="margin-top:35px;">On behalf of the entire CCHR community, I extend my heartfelt appreciation.</div>
		<div class="desc" style="margin-top:20px;">Sincerely,</div>
		<div class="desc" style="margin-top:20px;">
			<img width="100%" height="100%" style="max-width:200px;max-height:133px" src="<%=contextPath%>/assets/images/cchr/Frans-signature.jpg">
		</div>
		<div class="desc">Fran Andrews, Executive Director</div>
		<div class="desc" style="margin-top:20px;">
			<strong>P.S. Ask your employer if they offer a matching gift program — so you could double or triple your donation at no additional cost. Email us or call 1-800-869-2247 to check.</strong>
		</div>
		<div class="desc" style="margin-top:20px;font-size:30px;">
			<a href="#"><strong>Visit Our Website.</strong></a>
		</div>
	</div>
</div>

<div class="row desc" style="margin-top:100px;padding-bottom:30px;padding-top:30px;background-color:#D1CFD3; text-align:center;color:#000;font-size:15px">
	  <!-- Three columns of text below the carousel -->
		<p>&copy1995-2015 Citizens Commission on Human Rights (CCHR). All Rights Reserved.</p>
		<p>6616 Sunset Blvd. • Los Angeles, CA 90028</p>
		<p>1-800-869-2247</p>
		<p><a>www.CCHR.org</a></p>
		<p></p>
		<p>CCHR is a 501(c)3 nonprofit founded in 1969. Your donation to CCHR is tax deductible.</p>
		<p></p>
		<p>Click here for our privacy policy. If you prefer not to receive messages from us please click here.</p>
</div>
</body>
</html>