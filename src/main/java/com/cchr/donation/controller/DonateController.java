package com.cchr.donation.controller;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cchr.donation.AuthorizeCheckOut;
import com.cchr.donation.ConfigInfo;
import com.cchr.donation.DonationVO;
import com.cchr.donation.PaypalExpressCheckOut;
import com.cchr.donation.Product;
import com.cchr.donation.ResultEntity;
import com.cchr.donation.model.DonationRecord;
import com.cchr.donation.service.DonationRecordService;
import com.cchr.donation.service.NotificationService;
import com.cchr.donation.util.HtmlComponentUtil;

import net.authorize.api.contract.v1.CreditCardType;
import urn.ebay.api.PayPalAPI.CreateRecurringPaymentsProfileResponseType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.api.PayPalAPI.GetExpressCheckoutDetailsResponseType;
import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.eBLBaseComponents.ErrorType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsItemType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;

@Controller
@RequestMapping("/donate")
public class DonateController {
	@Autowired
	private NotificationService notificationService;
	@Autowired
	DonationRecordService donationRecordService;
	@Autowired
	private ConfigInfo configInfo;

	public static final String MONTHLY_DONATION = "MONTHLY_DONATION";
	public static final String ONE_TIME_DONATION = "ONE_TIME_DONATION";
//	public static final String PAYMENT_TYPE_PAYPAL = "usePaypal";
	private static final String PRODUCT_NAME_MONTHLY_DONATION = "Monthly Donation";
	private static final String PRODUCT_NAME_ONE_TIME_DONATION = "One-Time Donation";
	private static final String PAYMENT_TYPE_PAYPAL="Paypal";
	private static final String PAYMENT_TYPE_CREDIT_CARD="Creidt card";
	private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static final SimpleDateFormat df_thank_you = new SimpleDateFormat("MM/dd/yyyy");

	Logger logger = Logger.getLogger(DonateController.class);

	@RequestMapping(value = { "" })
	public String doDoneate(DonationRecord donationRecord, Model model, HttpServletRequest request) {
		logger.info("::Start doDoneate:: "+df.format(new Date()));
		ResultEntity result = new ResultEntity();
		String viewName = "error";

		if (donationRecord.getDonationAmt().compareTo(new BigDecimal(-1)) == 0) {
			if (donationRecord.getOtherAmount() != null) {
				donationRecord.setDonationAmt(donationRecord.getOtherAmount());
			} else {
				donationRecord.setDonationAmt(new BigDecimal(0));
			}
		}

		Product product = new Product();
		product.setPrice(donationRecord.getDonationAmt().toString());
		product.setQuantity(1);

		if (donationRecord.getDonationType() != null
				&& donationRecord.getDonationType().equalsIgnoreCase(MONTHLY_DONATION)) {
			donationRecord.setDonationType(MONTHLY_DONATION);
			product.setName(PRODUCT_NAME_MONTHLY_DONATION);
		} else {
			donationRecord.setDonationType(ONE_TIME_DONATION);
			product.setName(PRODUCT_NAME_ONE_TIME_DONATION);
		}

		if (donationRecord.isUsePaypal()) {
			logger.info("Start to use Paypal to do donate.......");
			donationRecord.setPaymentType(PAYMENT_TYPE_PAYPAL);
			result = paypalSetCheckOut(donationRecord, product);
			if (result.getCode().equalsIgnoreCase(ResultEntity.SUCCESS_CODE)) {
				logger.info("Start to go to Paypal site to do donate.......");
				viewName = "redirect:" + configInfo.getPaypalSite() + result.getToken();
			}
		} else {
			donationRecord.setPaymentType(PAYMENT_TYPE_CREDIT_CARD);
			AuthorizeCheckOut authorizeCheckOut = new AuthorizeCheckOut();

			CreditCardType creditCard = new CreditCardType();
			creditCard.setCardNumber(donationRecord.getCardNumber().replace(" ", ""));
			creditCard.setCardCode(donationRecord.getCardCode());
			creditCard.setExpirationDate(donationRecord.getExpirationDateMonth() + "/"
					+ donationRecord.getExpirationDateYear());
			// End SandBox Test

			if (donationRecord.getDonationType().equalsIgnoreCase(MONTHLY_DONATION)) {
				logger.info("Start to use authorize monthly way to do donate.......");
				result = authorizeCheckOut.createSubscription(configInfo.getAuthorizeEnv(),configInfo.getLoginID(), configInfo.getTransactionKey(),
						creditCard, donationRecord, product);
			} else {
				logger.info("Start to use authorize normal way to do donate.......");
				result = authorizeCheckOut.creditCardCheckOut(configInfo.getAuthorizeEnv(),configInfo.getLoginID(), configInfo.getTransactionKey(),
						creditCard, donationRecord, product);
			}

			if (result.getCode().equalsIgnoreCase(ResultEntity.SUCCESS_CODE)) {
				logger.info("Use authorize to do donate successfully......");
				donationRecord.setDonationDate(new Date());
				try {
					String cardNumber= donationRecord.getCardNumber();
					String cardNumberSuffix = cardNumber.substring(cardNumber.length()-4);
					String maskedCardNumber = cardNumber.replaceAll("\\d", "*").substring(0,cardNumber.length()-4)+cardNumberSuffix;
					DonationVO vo = new DonationVO();
					vo.setFirstName(donationRecord.getFirstName());
					vo.setLastName(donationRecord.getLastName());
					vo.setCardNumber(maskedCardNumber);
					vo.setDonationAmount(donationRecord.getDonationAmt().toString());
					vo.setTransactionDate(df_thank_you.format(donationRecord.getDonationDate()));					
					donationRecordService.insertDonationRecord(donationRecord);
					logger.info("Save authorize donate record to database successfully.......");
					sendEmail(donationRecord, vo);
					sendNotification(donationRecord);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("fail to save this donation record, email: "+donationRecord.getEmail()+" , time: "+df.format(new Date())+", exception: "+e);
				}
				// Thanks Page
				viewName = "redirect:/donate/thankyou";
			}
		}

		if(result.getCode().equalsIgnoreCase(ResultEntity.FAIL_CODE)){
			if(result.getMessage()==null || result.getMessage().size()==0){
				List<String> list = new ArrayList<String>();
				list.add("Fail to process this donation, please try it again!");
				result.setMessage(list);
			}
			model.addAttribute("message",result.getMessage());
			viewName = "error";
		}
		
		logger.info("::End doDoneate:: "+ df.format(new Date()));
		return viewName;
	}

	@RequestMapping(value = { "checkOut" })
	public String paypalCheckOut(String token, Model model, HttpServletRequest request,
			RedirectAttributes redirectAttr) {
		logger.info("::Start paypalCheckOut:: "+ df.format(new Date()));

		ResultEntity result = new ResultEntity();
		result.setCode(ResultEntity.FAIL_CODE);
		List<String> errorList = new ArrayList<String>();
		String payler="";

		String viewName = configInfo.getMainUrl();
		DonationRecord donationRecord = new DonationRecord();

		PaypalExpressCheckOut paypalExpressCheckOut = new PaypalExpressCheckOut();
//		PayPalAPIInterfaceServiceService service = this.getService();
		PayPalAPIInterfaceServiceService service;

		GetExpressCheckoutDetailsResponseType responseType;
		try {
			Resource resource =new ClassPathResource("conf/sdk.properties");
			service = new PayPalAPIInterfaceServiceService(resource.getFile());
			responseType = paypalExpressCheckOut.getExpressCheckout(token, service);
			if (responseType != null) {
				if (responseType.getAck().toString().equalsIgnoreCase("SUCCESS")) {
					payler =responseType.getGetExpressCheckoutDetailsResponseDetails().getPayerInfo().getPayer();
					
					List<PaymentDetailsType> deatilsList = responseType.getGetExpressCheckoutDetailsResponseDetails()
							.getPaymentDetails();
					PaymentDetailsType detailsType = deatilsList.get(0);
					PaymentDetailsItemType itemType = detailsType.getPaymentDetailsItem().get(0);
					Product product = new Product();
					product.setName(itemType.getName());
					product.setPrice(itemType.getAmount().getValue());
					product.setQuantity(1);

					donationRecord.setDonationAmt(new BigDecimal(product.getPrice()));
					String type = request.getParameter("type");
					
					if (type != null && type.equalsIgnoreCase("monthly")) {
						donationRecord.setDonationType(MONTHLY_DONATION);
						CreateRecurringPaymentsProfileResponseType resp = paypalExpressCheckOut
								.createRecurringPaymentsProfile(token, product, service, 0);
						if (resp != null) {
							if (resp.getAck().toString().equalsIgnoreCase("SUCCESS")) {
								result.setCode(ResultEntity.SUCCESS_CODE);
							} else {
								List<ErrorType> errors = resp.getErrors();
								if (errors != null) {
									result.setCode(ResultEntity.FAIL_CODE);
									for (ErrorType error : errors) {
										errorList.add(error.getLongMessage());
									}
								}
							}
						} else {
							result.setCode(ResultEntity.FAIL_CODE);
							logger.error("Faild  doExpressCheckout from paypal");
							errorList.add("Faild  creat monthly donation from paypal");
						}
					} else {
						DoExpressCheckoutPaymentResponseType doResp = paypalExpressCheckOut.doExpressCheckout(token,
								service, responseType);
						
						if (doResp != null) {
							if (doResp.getAck().toString().equalsIgnoreCase("SUCCESS")) {
								donationRecord.setDonationType(ONE_TIME_DONATION);
								result.setCode(ResultEntity.SUCCESS_CODE);
							}else {
								List<ErrorType> errors = doResp.getErrors();
								if (errors != null) {
									result.setCode(ResultEntity.FAIL_CODE);
									for (ErrorType error : errors) {
										errorList.add(error.getLongMessage());
									}
								}
							}
						} else {
							result.setCode(ResultEntity.FAIL_CODE);
							logger.error("Faild  doExpressCheckout from paypal");
							errorList.add("Faild  doExpressCheckout from paypal");
						}
					}
				} else {
					List<ErrorType> errors = responseType.getErrors();
					if (errors != null) {
						result.setCode(ResultEntity.FAIL_CODE);
						for (ErrorType error : errors) {
							errorList.add(error.getLongMessage());
						}
					}
				}
			} else {
				result.setCode(ResultEntity.FAIL_CODE);
				logger.error("Faild  getExpressCheckout from paypal");
				errorList.add("Faild  getExpressCheckout from paypal");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("fail to getExpressCheckout from paypal, email: "+donationRecord.getEmail()+" , time: "+df.format(new Date())+", exception: "+e);
			result.setCode(ResultEntity.FAIL_CODE);
			errorList.add(e.getMessage());
		}

		if (result.getCode().equalsIgnoreCase(ResultEntity.SUCCESS_CODE)) {
			logger.info("Use Paypal to do donate successfully.......");
			getBillInfo(donationRecord, request);

			donationRecord.setDonationDate(new Date());
			donationRecord.setPaymentType(PAYMENT_TYPE_PAYPAL);
			donationRecordService.insertDonationRecord(donationRecord);
			try {
				DonationVO vo = new DonationVO();
				vo.setFirstName(donationRecord.getFirstName());
				vo.setLastName(donationRecord.getLastName());
				vo.setPaymentType(payler);
				vo.setDonationAmount(donationRecord.getDonationAmt().toString());
				vo.setTransactionDate(df_thank_you.format(donationRecord.getDonationDate()));
				sendEmail(donationRecord, vo);
				sendNotification(donationRecord);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("fail to save this donation record, email: "+donationRecord.getEmail()+" , time: "+df.format(new Date())+", exception: "+e);
			}
			viewName = "redirect:/donate/thankyou";
		} else {

			if (errorList.size() == 0) {
				errorList.add("Donate failed, Please check your Paypal Account and try again!");
			}
			result.setMessage(errorList);
			redirectAttr.addFlashAttribute("message", result.getMessage());
			viewName = "redirect:/error";
		}

		logger.info("::End paypalCheckOut:: "+df.format(new Date()));
		return viewName;
	}

	public ResultEntity paypalSetCheckOut(DonationRecord donationRecord, Product product) {
		logger.info("::Start paypalSetCheckOut:: "+df.format(new Date()));

		ResultEntity result = new ResultEntity();
		result.setCode(ResultEntity.FAIL_CODE);
		List<String> errorList = new ArrayList<String>();

		PaypalExpressCheckOut paypalExpressCheckOut = new PaypalExpressCheckOut();
//		PayPalAPIInterfaceServiceService service = this.getService();
		PayPalAPIInterfaceServiceService service ;

		String checkOutUrl = getCheckOutURL(donationRecord);

		SetExpressCheckoutResponseType setExpressCheckoutResponse;
		try {
			Resource resource =new ClassPathResource("conf/sdk.properties");
			service = new PayPalAPIInterfaceServiceService(resource.getFile());
			if (product.getName().equalsIgnoreCase(PRODUCT_NAME_MONTHLY_DONATION)) {
				checkOutUrl += "&type=monthly";
				setExpressCheckoutResponse = paypalExpressCheckOut.setExpressCheckoutForRecurringPayments(product,
						service, checkOutUrl, configInfo.getMainUrl());
			} else {
				setExpressCheckoutResponse = paypalExpressCheckOut.setExpressCheckout(product, service, checkOutUrl,
						configInfo.getMainUrl());
			}
			if (setExpressCheckoutResponse != null) {
				if (setExpressCheckoutResponse.getAck().toString().equalsIgnoreCase("SUCCESS")) {
					result.setCode(ResultEntity.SUCCESS_CODE);
					result.setToken(setExpressCheckoutResponse.getToken());
					// return "redirect:" +
					// configInfo.getPaypalSite()+setExpressCheckoutResponse.getToken();
				} else {
					if (setExpressCheckoutResponse.getErrors() != null) {
						List<ErrorType> errors = setExpressCheckoutResponse.getErrors();
						for (ErrorType error : errors) {
							errorList.add(error.getLongMessage());
						}
						result.setCode(ResultEntity.FAIL_CODE);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("fail to setExpressCheckout: exception: "+e+":: "+df.format(new Date()));
			result.setCode(ResultEntity.FAIL_CODE);
			errorList.add(e.getMessage());
		}

		if (result.getCode().equalsIgnoreCase(ResultEntity.FAIL_CODE)) {
			if (errorList.size() == 0) {
				errorList.add("Failed connect to paypal site, Please try it latter!");
			}
			result.setMessage(errorList);
		}

		logger.info("::End paypalSetCheckOut:: "+ df.format(new Date()));
		return result;
	}

	@RequestMapping(value = { "testEmail" })
	public void testEmail(HttpServletRequest request) throws Exception {
		/*
		String fromEmaill = "Test@smtp.sendgrid.net";
//		String toEmail = "43642483@qq.com";
//		String ccEmail = "zfx_zzu@126.com";
		String toEmail = "wang_wenchao@yahoo.com";
//		String toEmail = "wangwengcn@163.com";
		String subject = "Test ACMS Email";
		StringBuilder mailContent = new StringBuilder();
		mailContent.append(HtmlComponentUtil.initThankyouMailPage("zhangsan", "100", "10/30/2015", "zhang", "san", "paypal", "00000001111", "100"));
		notificationService.sendHtmlMail(fromEmaill, toEmail, subject, mailContent.toString());
		*/
		
		/*
		 * test notification*/
		DonationRecord donationRecord = new DonationRecord();
		donationRecord.setFirstName("Fanxin");
		donationRecord.setLastName("Zeng");
		donationRecord.setPhone("1333333333");
		donationRecord.setAddress("China Hangzhou");
		donationRecord.setEmail("562719461@qq.com");
		donationRecord.setDonationDate(new Date());
		donationRecord.setDonationAmt(new BigDecimal("25"));
		donationRecord.setDonationType(ONE_TIME_DONATION);
		donationRecord.setPaymentType("Paypal");
		donationRecord.setTshirtSize("Small");
		donationRecord.setEffort("2");
		donationRecord.setZip("310000");
		donationRecord.setState("Zhejiang");
		donationRecord.setCity("Hangzhou");
		donationRecord.setCountry("China");
		donationRecord.setAddress("xxxxxx1");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		StringBuffer emailContent = new StringBuffer();
		emailContent.append("<div>"+
	"Great news!  CCHR International campaign just received a donation from your campaign website!"+
	"<br><br>"+
	"Here is the info about the donor:</div>");
		emailContent.append("<div style='padding-left:50px;margin-top:15px'>");
		emailContent.append("Name: "+donationRecord.getFirstName()+","+donationRecord.getLastName()+"<br><br>");
		
		emailContent.append("Address: "+(donationRecord.getAddress()==null?"":donationRecord.getAddress())+"<br><br>");
		emailContent.append("         "+(donationRecord.getAddress2()==null?"":donationRecord.getAddress2())+"<br><br>");
		emailContent.append("City: "+(donationRecord.getCity()==null?"":donationRecord.getCity())+"<br><br>");
		emailContent.append("State/Province: "+(donationRecord.getState()==null?"":donationRecord.getState())+"<br><br>");
		emailContent.append("Country: "+(donationRecord.getCountry()==null?"":donationRecord.getCountry())+"<br><br>");
		emailContent.append("Zip: "+(donationRecord.getZip()==null?"":donationRecord.getZip())+"<br><br>");
		
		emailContent.append("Phone: "+(donationRecord.getPhone()==null?"":donationRecord.getPhone())+"<br><br>");
		emailContent.append("Email: "+(donationRecord.getEmail()==null?"":donationRecord.getEmail())+"<br><br>");
		emailContent.append("Donation Date: "+(donationRecord.getDonationDate()==null?"":sdf.format(donationRecord.getDonationDate()))+"<br><br>");
		emailContent.append("Donation Amount:  $"+donationRecord.getDonationAmt()+" <br><br>");
		
		if(MONTHLY_DONATION.equals(donationRecord.getDonationType())){
			emailContent.append("Donation Type: "+PRODUCT_NAME_MONTHLY_DONATION+" <br><br>");
		}else if(ONE_TIME_DONATION.equals(donationRecord.getDonationType())){
			emailContent.append("Donation Type: "+PRODUCT_NAME_ONE_TIME_DONATION+" <br><br>");
		}else{
			emailContent.append("Donation Type: "+donationRecord.getDonationType()+"<br><br>");
		}
		
		emailContent.append("Payment Type: "+donationRecord.getPaymentType()+"<br><br>");
		if(donationRecord.getTshirtSize() != null && donationRecord.getTshirtSize().length()>0 && !donationRecord.getTshirtSize().equals("-1") && !donationRecord.getTshirtSize().equalsIgnoreCase("null")){
			emailContent.append("T-Shirt Size: "+donationRecord.getTshirtSize()+"<br><br>");
		}else{
			emailContent.append("T-Shirt Size: <br><br>");
		}
		emailContent.append("Effort Type: effort"+donationRecord.getEffort()+"<br><br>");
		emailContent.append("</div>");
		String subject = "Donation - 2015 Email Campaign";
		logger.info(emailContent.toString());
	    notificationService.sendHtmlMailWithoutAttached("Test@smtp.sendgrid.net", "562719461@qq.com;43642483@qq.com", subject, emailContent.toString());
	}

	@RequestMapping(value = { "thankyou" })
	public String thankyouPage(HttpServletRequest request) throws Exception {
		return "thankyou";
	}

	@RequestMapping(value = { "thankYouMail" })
	public String thankYouMail(HttpServletRequest request) throws Exception {
		return "thankYouMail";
	}
		
	public void sendEmail(DonationRecord donationRecord, DonationVO vo) throws Exception {
		String subject = "Donation Thank You/Confirmation";
		DecimalFormat usFormat = new DecimalFormat("###,###");
		String dona = vo.getDonationAmount();
		if(dona.contains(".")){
			dona = usFormat.format(new Integer(dona.substring(0, dona.indexOf(".")))) + dona.substring(dona.indexOf("."));
		}else{
			dona = usFormat.format(new Integer(dona));
		}
		StringBuilder mailContent = HtmlComponentUtil.initThankyouMailPage(vo.getFirstName(), vo.getDonationAmount(), vo.getTransactionDate(), vo.getFirstName(), vo.getLastName(), vo.getPaymentType(), vo.getCardNumber(), dona, donationRecord);
		notificationService.sendHtmlMail(configInfo.getFromEmail(), donationRecord.getEmail(), subject, mailContent.toString());
	}
	
	public void sendNotification(DonationRecord donationRecord) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		DecimalFormat usFormat = new DecimalFormat("###,###");   
		StringBuffer emailContent = new StringBuffer();
		emailContent.append("<div>"+
				"Great news!  CCHR International campaign just received a donation from your campaign website!"+
				"<br><br>"+
				"Here is the info about the donor:</div>");
		emailContent.append("<div style='padding-left:50px;margin-top:15px'>");
		emailContent.append("Name: "+donationRecord.getFirstName()+","+donationRecord.getLastName()+"<br><br>");
		
		emailContent.append("Address: "+(donationRecord.getAddress()==null?"":donationRecord.getAddress())+"<br><br>");
		emailContent.append("         "+(donationRecord.getAddress2()==null?"":donationRecord.getAddress2())+"<br><br>");
		emailContent.append("City: "+(donationRecord.getCity()==null?"":donationRecord.getCity())+"<br><br>");
		emailContent.append("State/Province: "+(donationRecord.getState()==null?"":donationRecord.getState())+"<br><br>");
		emailContent.append("Country: "+(donationRecord.getCountry()==null?"":donationRecord.getCountry())+"<br><br>");
		emailContent.append("Zip: "+(donationRecord.getZip()==null?"":donationRecord.getZip())+"<br><br>");
		
		emailContent.append("Phone: "+(donationRecord.getPhone()==null?"":donationRecord.getPhone())+"<br><br>");
		emailContent.append("Email: "+(donationRecord.getEmail()==null?"":donationRecord.getEmail())+"<br><br>");
		emailContent.append("Donation Date: "+(donationRecord.getDonationDate()==null?"":sdf.format(donationRecord.getDonationDate()))+"<br><br>");
		if(donationRecord.getDonationAmt()==null){
			emailContent.append("Donation Amount:  $0 <br><br>");
		}else{
			String amtStr = donationRecord.getDonationAmt().toString();
			if(amtStr.contains(".")){
				emailContent.append("Donation Amount:  $"+(usFormat.format(new Integer(amtStr.substring(0, amtStr.indexOf("."))))) +amtStr.substring(amtStr.indexOf("."))+" <br><br>");
			}else{
				emailContent.append("Donation Amount:  $"+(usFormat.format(new Integer(amtStr)))+" <br><br>");
			}
		}
		
		if(MONTHLY_DONATION.equals(donationRecord.getDonationType())){
			emailContent.append("Donation Type: "+PRODUCT_NAME_MONTHLY_DONATION+" <br><br>");
		}else if(ONE_TIME_DONATION.equals(donationRecord.getDonationType())){
			emailContent.append("Donation Type: "+PRODUCT_NAME_ONE_TIME_DONATION+" <br><br>");
		}else{
			emailContent.append("Donation Type: "+donationRecord.getDonationType()+"<br><br>");
		}
		
		emailContent.append("Payment Type: "+donationRecord.getPaymentType()+"<br><br>");
		if(donationRecord.getTshirtSize() != null && donationRecord.getTshirtSize().length()>0 && !donationRecord.getTshirtSize().equals("-1") && !donationRecord.getTshirtSize().equalsIgnoreCase("null")){
			emailContent.append("T-Shirt Size: "+donationRecord.getTshirtSize()+"<br><br>");
		}else{
			emailContent.append("T-Shirt Size: <br><br>");
		}
		emailContent.append("Effort Type: effort"+donationRecord.getEffort()+"<br><br>");
		emailContent.append("</div>");
		
		String subject = "Donation - 2015 Email Campaign";
		String fromEmaill = configInfo.getFromEmail();
		notificationService.sendHtmlMailWithoutAttached(fromEmaill, configInfo.getNotifyTo(), subject, emailContent.toString());
	}

	public String getCheckOutURL(DonationRecord donationRecord) {

		StringBuffer sb = new StringBuffer(configInfo.getCheckOutUrl());
		sb.append("?firstName=").append(donationRecord.getFirstName());
		sb.append("&lastName=").append(donationRecord.getLastName());
		sb.append("&address=").append(donationRecord.getAddress());
		sb.append("&address2=").append(donationRecord.getAddress2());
		sb.append("&city=").append(donationRecord.getCity());
		sb.append("&state=").append(donationRecord.getState());
		sb.append("&zip=").append(donationRecord.getZip());
		sb.append("&country=").append(donationRecord.getCountry());
		sb.append("&size=").append(donationRecord.getTshirtSize());
		sb.append("&effort=").append(donationRecord.getEffort());
		sb.append("&phone=").append(donationRecord.getPhone());
		sb.append("&email=").append(donationRecord.getEmail());

		return sb.toString();
	}

	public void getBillInfo(DonationRecord donationRecord, HttpServletRequest request) {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String address = request.getParameter("address");
		String address2 = request.getParameter("address2");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String zip = request.getParameter("zip");
		String country = request.getParameter("country");
		String size = request.getParameter("size");
		String effort = request.getParameter("effort");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");

		if (firstName != null)
			donationRecord.setFirstName(firstName);
		if (lastName != null)
			donationRecord.setLastName(lastName);
		if (address != null)
			donationRecord.setAddress(address);
		if (address2 != null)
			donationRecord.setAddress2(address2);
		if (city != null)
			donationRecord.setCity(city);
		if (state != null)
			donationRecord.setState(state);
		if (zip!=null)
			donationRecord.setZip(zip);
		if (country != null)
			donationRecord.setCountry(country);
		if (size != null && !StringUtils.isEmpty(size) && !size.equalsIgnoreCase("null"))
			donationRecord.setTshirtSize(size);
		if (effort != null)
			donationRecord.setEffort(effort);
		if (phone != null)
			donationRecord.setPhone(phone);
		if (email != null)
			donationRecord.setEmail(email);
	}
}
