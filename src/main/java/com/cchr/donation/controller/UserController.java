/**
 * 
 */
package com.cchr.donation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cchr.donation.model.User;
import com.cchr.donation.service.UserService;

/**
 * @author Fanxin Zeng
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
    private UserService userService;
	private ObjectMapper om = new ObjectMapper();
    
    @RequestMapping("/index")
    public String index(){
        return "sys/user_list";
    }
    
    @RequestMapping(value="/list")
    public void getDoctors(HttpServletResponse resp) throws IOException{
    	JsonGenerator jg = om.getJsonFactory().createJsonGenerator(resp.getOutputStream());
    	List<User> data = userService.getAllUser();
    	jg.writeObject(data);
    	jg.close();
    }
    
    @RequestMapping(value="/regist")
    public void regist(HttpServletResponse resp) throws IOException{
    	
    }
    
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String loginPage(HttpServletRequest request, HttpServletResponse resp) throws IOException{
    	return "login";
    }
    
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public String login(HttpServletRequest request, HttpServletResponse resp) throws IOException{
    	request.getSession().setAttribute("userName", "");
    	return "redirect:/";
    }
    
    @RequestMapping(value="/save")
    public void save(HttpServletResponse resp) throws IOException{
    	
    }
    
    @RequestMapping(value="/delete")
    public void delete(HttpServletResponse resp) throws IOException{
    	
    }
}
