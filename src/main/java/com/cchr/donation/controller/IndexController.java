/**
 * 
 */
package com.cchr.donation.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cchr.donation.util.HtmlComponentUtil;

/**
 * @author Fanxin Zeng
 *
 */
@Controller
public class IndexController {
	
    
	public static String[] COUNTRY = {"Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Antigua and Barbuda","Argentina","Armenia","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Colombia","Comoros","Congo, Democratic Republic of the","Congo, Republic of the","Costa Rica","C涔坱e d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Faroe Islands","Fiji","Finland","France","French Polynesia","Gabon","Gambia","Georgia","Germany","Ghana","Greece","Greenland","Grenada","Guam","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati","North Korea","South Korea","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestine, State of","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Romania","Russia","Rwanda","Saint Kitts and Nevis","Saint Lucia","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","Spain","Sri Lanka","Sudan","Sudan, South","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands, British","Virgin Islands, U.S.","Yemen","Zambia","Zimbabwe"};
	public static String[] MONTH = {"01","02","03","04","05","06","07","08","09","10","11","12"};
	public static String[] YEAR = new String[20];
	//public static double[] AMOUNT_LEVEL_ARRAY = {19,79,109,350,750};
	//public static double[][] DONATE_AMOUT_ARRAY = {{25,50,100},{100,200,300},{200,300,500},{500,750,1000},{1000,1500,2000},{2000,2500,5000}};
	public static double[] AMOUNT_LEVEL_ARRAY = {24,49,99,199,299,499,749,999,1499,1999,4999};
	public static double[][] DONATE_AMOUT_ARRAY = {{25,50,100},{50,100,200},{100,200,300},{200,300,500},{300,500,750},{500,750,1000},{750,1000,1500},{1000,1500,2000},{1500,2000,2500},{2000,2500,5000},{2500,3500,5000},{5000,7500,10000}};
	public static final DecimalFormat df = new DecimalFormat("#,###,###,###");
   
	Logger logger = Logger.getLogger(IndexController.class);
	    
	@RequestMapping("/index")
	public String index(String lp, String effort, String spMailingID, String spReportId, Model model,HttpServletRequest request, HttpServletResponse response)
    {
		// 默认界面
		if(StringUtils.isEmpty(effort))
		{
			effort = "fb1";
		}
		logger.info("---welcome---");
		response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");   
		response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");   
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");   
		response.setHeader("Pragma", "no-cache");   
    	Calendar calendar = Calendar.getInstance();
    	Integer currYear = calendar.get(Calendar.YEAR);
    	double[] donateAmount;
    	List<String> amountList= new ArrayList<String>();
    	DecimalFormat df = new DecimalFormat("#,###");

    	for(int i=0; i<YEAR.length; i++)
    	{
    		YEAR[i] = currYear.toString();
    		currYear ++;
    	}
    	    	
    	int exAmount=0;
    	// 过滤1A,1B,3A,3B的金额
    	if(effort.equalsIgnoreCase("1a") || effort.equalsIgnoreCase("1b") || effort.equalsIgnoreCase("3a") || effort.equalsIgnoreCase("3b"))
    	{
    		lp = "0";
    	}
    	if(lp!=null && lp.length()>0){
    	    try{
    	    	exAmount= Integer.valueOf(lp);
    	    }catch(Exception e){
    	    	exAmount = 0;
    	    }
    	    donateAmount= getDonateAmount(exAmount);
    	}else{
    	    donateAmount= DONATE_AMOUT_ARRAY[0];
    	}
        
    	
    	for(double amout:donateAmount){
		  String str = df.format(amout);
		  amountList.add(str);
    	}
    	
    	request.setAttribute("MONTH", HtmlComponentUtil.buildList(MONTH));
    	request.setAttribute("YEAR", HtmlComponentUtil.buildList(YEAR));
    	request.setAttribute("countryDatas", HtmlComponentUtil.buildList(COUNTRY));
    	model.addAttribute("donateAmount",amountList);
    	model.addAttribute("exAmount", Integer.valueOf(exAmount));
    	DecimalFormat usFormat = new DecimalFormat("###,###"); 
    	model.addAttribute("lp", lp);
    	model.addAttribute("lps", usFormat.format(exAmount));
    	model.addAttribute("effort", effort.toUpperCase());
    	if(spMailingID!=null && spMailingID.length()>0){
    		model.addAttribute("mailingID", spMailingID.toUpperCase());
    	}else {
    		model.addAttribute("mailingID", "");
    	}
    	
    	if(spReportId!=null && spReportId.length()>0){
    		model.addAttribute("reportID", spReportId.toUpperCase());
    	}else{
    		model.addAttribute("reportID", "");
    	}
    
    	return "index." + effort.toLowerCase();
    }
    
    @RequestMapping("/main")
    public String main(HttpServletRequest request)
    {
        return "main";
    }
    
    @RequestMapping("/error")
    public String error(HttpServletRequest request)
    {
        return "error";
    }
    
    @RequestMapping("/")
    public String defaultIndex(HttpServletRequest request)
    {
        return "forward:/index?effort=fb1";
    }
    
    public double[] getDonateAmount(int exAmount){
	for(int i=0;i<AMOUNT_LEVEL_ARRAY.length;i++){
	    if(exAmount<=AMOUNT_LEVEL_ARRAY[i]){
		return DONATE_AMOUT_ARRAY[i];
	    }
	}
	
	return DONATE_AMOUT_ARRAY[11];	
    }
    
}
