package com.cchr.donation.service;


public interface NotificationService {
	public void sendSimpleMail(String fromEmaill, String toEmail, String ccEmail, String subject, String mailContent)  throws Exception;
	public void sendSimpleMail(String fromEmaill, String toEmail, String subject, String mailContent)  throws Exception;
	public void sendHtmlMail(String fromEmaill, String toEmail, String subject, String mailContent)  throws Exception;
	public void sendHtmlMailWithoutAttached(String fromEmaill, String toEmail, String subject, String mailContent)  throws Exception;
}
