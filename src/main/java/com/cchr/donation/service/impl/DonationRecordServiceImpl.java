package com.cchr.donation.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cchr.donation.dao.DonationRecordDAO;
import com.cchr.donation.model.DonationRecord;
import com.cchr.donation.service.DonationRecordService;

@Service
public class DonationRecordServiceImpl implements DonationRecordService{
	
	@Autowired
	private DonationRecordDAO donationRecordDAO;

	public int insertDonationRecord(DonationRecord donationRecord) {
		return donationRecordDAO.insertDonationRecord(donationRecord);
	}

	public int updateDonationRecord(DonationRecord donationRecord) {
		return donationRecordDAO.updateDonationRecord(donationRecord);
	}

	public DonationRecord queryDonationRecordById(int id) {
		return donationRecordDAO.queryDonationRecordById(id);
	}

	public int deleteDonationRecordById(int id) {
		return donationRecordDAO.deleteDonationRecordById(id);
	}

	public List<DonationRecord> getAllDonationRecord() {
		return donationRecordDAO.getAllDonationRecord();
	}

}
