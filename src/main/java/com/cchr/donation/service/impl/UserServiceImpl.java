/**
 * 
 */
package com.cchr.donation.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cchr.donation.dao.UserDAO;
import com.cchr.donation.model.User;
import com.cchr.donation.service.UserService;

/**
 * @author Zeng
 *
 */
@Service
public class UserServiceImpl implements UserService{
 
    @Autowired
    private UserDAO userDAO;
     
    public int insertUser(User user) {
        return userDAO.insertUser(user);
    }
    
    public int updateUser(User user){
    	return userDAO.updateUser(user);
    }
    
    public int deleteUser(int id){
    	return userDAO.deleteUser(id);
    }
    
    public User getUserById(int id){
    	return userDAO.getUserById(id);
    }
    
    public User getUserByUserName(String userName){
    	return userDAO.getUserByUserName(userName);
    }

	public List<User> getAllUser() {
		return userDAO.getAllUser();
	}
}
