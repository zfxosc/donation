package com.cchr.donation.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.cchr.donation.service.NotificationService;



@Service
public class NotificationServiceImpl implements NotificationService{

	@Autowired
	private JavaMailSender javaMailSender;

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public void sendSimpleMail(String fromEmaill, String toEmail, String ccEmail, String subject, String mailContent)  throws Exception{
		
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setFrom(fromEmaill);
		simpleMailMessage.setTo(toEmail);
		simpleMailMessage.setCc(ccEmail);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(mailContent);
		javaMailSender.send(simpleMailMessage);
		logger.info("sendSimpleMail type1 successfully");
	}
	
	public void sendSimpleMail(String fromEmaill, String toEmail, String subject, String mailContent)  throws Exception{
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setFrom(fromEmaill);
		simpleMailMessage.setTo(toEmail);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(mailContent);
		javaMailSender.send(simpleMailMessage);
		logger.info("sendSimpleMail without CC successfully");
	}
	
	public void sendHtmlMail(String fromEmaill, String toEmail, String subject, String mailContent)  throws Exception{
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
		messageHelper.setFrom(fromEmaill);
		messageHelper.setTo(toEmail);
		messageHelper.setSubject(subject);
		messageHelper.setText(mailContent, true);
		ClassPathResource cchr = new ClassPathResource("/mail/images/cchr.png");  
		messageHelper.addInline("cchr", cchr);  
		ClassPathResource signature = new ClassPathResource("/mail/images/frans-signature.jpg");  
		messageHelper.addInline("signature", signature);
		javaMailSender.send(message);
		logger.info("sendHtmlMail type3 successfully");
	}

	public void sendHtmlMailWithoutAttached(String fromEmaill, String toEmail, String subject, String mailContent)
			throws Exception {
		String[] toEmails = toEmail.split(";");
		List<InternetAddress> toList = new ArrayList<InternetAddress>();
		for(String to : toEmails)
		{
			try{
				InternetAddress toAddress = new InternetAddress(to);
				toList.add(toAddress);
			}catch(Exception e)
			{
				logger.error("format InternetAddress error: " + to);
			}
		}
		MimeMessage message = javaMailSender.createMimeMessage();
		message.setSentDate(new Date());
		InternetAddress fromAddress = new InternetAddress(fromEmaill);
		message.setFrom(fromAddress);
		message.setRecipients(Message.RecipientType.TO, toList.toArray(new InternetAddress[toList.size()]));
		message.setSubject(subject);
		message.setContent(mailContent, "text/html");
		javaMailSender.send(message);
		logger.info("sendHtmlMail type4 successfully");
	}

}
