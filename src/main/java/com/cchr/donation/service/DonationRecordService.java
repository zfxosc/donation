package com.cchr.donation.service;

import java.util.List;

import com.cchr.donation.model.DonationRecord;

/**
 * @author zeng
 *
 */
public interface DonationRecordService {
	
	/**
     * @param donationRecord
     * @return
     */
    public int insertDonationRecord(DonationRecord donationRecord);
    
	/**
     * @param donationRecord
     * @return
     */
    public int updateDonationRecord(DonationRecord donationRecord);
    
	/**
     * @param id
     * @return
     */
    public DonationRecord queryDonationRecordById(int id);
    
	
    public int deleteDonationRecordById(int id);
    
    public List<DonationRecord> getAllDonationRecord();
}
