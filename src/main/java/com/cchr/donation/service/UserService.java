/**
 * 
 */
package com.cchr.donation.service;

import java.util.List;

import com.cchr.donation.model.User;

/**
 * @author Zeng
 *
 */
public interface UserService {
	
	public int insertUser(User user);
	
	public int updateUser(User user);
	
	public int deleteUser(int id);
	
    public User getUserById(int id);
    
    public User getUserByUserName(String userName);

	public List<User> getAllUser();
}
