/**
 * 
 */
package com.cchr.donation.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Zeng
 * 
 */

public class DonationRecord {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;
    private String country;
    private String zip;
    private String address;
    private String address2;
    private Date donationDate;
    private String donationType;
    private BigDecimal donationAmt;
    private BigDecimal otherAmount;
    private String tshirtSize;
    private String phone;

    private boolean usePaypal;
    private boolean isMonthlyDonate;
    private String paymentType;
    private String mailingID;
    private String reportID;
    private String cardNumber;
    private String expirationDateMonth;
    private String expirationDateYear;
    private String cardCode;
    private String effort;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public String getZip() {
	return zip;
    }

    public void setZip(String zip) {
	this.zip = zip;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }
    
    public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public Date getDonationDate() {
	return donationDate;
    }

    public void setDonationDate(Date donationDate) {
	this.donationDate = donationDate;
    }

    public String getDonationType() {
	return donationType;
    }

    public void setDonationType(String donationType) {
	this.donationType = donationType;
    }

    public BigDecimal getDonationAmt() {
	return donationAmt;
    }

    public void setDonationAmt(BigDecimal donationAmt) {
	this.donationAmt = donationAmt;
    }

    public BigDecimal getOtherAmount() {
        return otherAmount;
    }

    public void setOtherAmount(BigDecimal otherAmount) {
        this.otherAmount = otherAmount;
    }

    public String getTshirtSize() {
	return tshirtSize;
    }

    public void setTshirtSize(String tshirtSize) {
	this.tshirtSize = tshirtSize;
    }

    public String getPaymentType() {
	return paymentType;
    }

    public void setPaymentType(String paymentType) {
	this.paymentType = paymentType;
    }

    public boolean isMonthlyDonate() {
	return isMonthlyDonate;
    }

    public void setMonthlyDonate(boolean isMonthlyDonate) {
	this.isMonthlyDonate = isMonthlyDonate;
    }

    public String getCardNumber() {
	return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
	this.cardNumber = cardNumber;
    }

    public String getExpirationDateMonth() {
	return expirationDateMonth;
    }

    public void setExpirationDateMonth(String expirationDateMonth) {
	this.expirationDateMonth = expirationDateMonth;
    }

    public String getExpirationDateYear() {
	return expirationDateYear;
    }

    public void setExpirationDateYear(String expirationDateYear) {
	this.expirationDateYear = expirationDateYear;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public String getCardCode() {
	return cardCode;
    }

    public void setCardCode(String cardCode) {
	this.cardCode = cardCode;
    }

    public boolean isUsePaypal() {
	return usePaypal;
    }

    public void setUsePaypal(boolean usePaypal) {
	this.usePaypal = usePaypal;
    }

	public String getEffort() {
		return effort;
	}

	public void setEffort(String effort) {
		this.effort = effort;
	}

	public String getMailingID() {
		return mailingID;
	}

	public void setMailingID(String mailingID) {
		this.mailingID = mailingID;
	}

	public String getReportID() {
		return reportID;
	}

	public void setReportID(String reportID) {
		this.reportID = reportID;
	}
}
