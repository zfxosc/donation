package com.cchr.donation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("configInfo")
public class ConfigInfo {

    @Value("${site.home.url}")
    private String mainUrl;

    @Value("${site.checkout.url}")
    private String checkOutUrl;

    @Value("${authorize.api.login.id}")
    private String loginID;

    @Value("${authorize.api.transaction.key}")
    private String transactionKey;

    @Value("${from.email}")
    private String fromEmail;
    @Value("${index.youtube.url}")
    private String indexYoutubeUrl;
    @Value("${paypal.paymengt.site}")
    private String paypalSite;
    @Value("${authorize.api.env}")
    private String authorizeEnv;    
    @Value("${notify.to}")
    private String notifyTo;    
    @Value("${notify.subject}")
    private String notifySubject;

    public String getMainUrl() {
	return mainUrl;
    }

    public void setMainUrl(String mainUrl) {
	this.mainUrl = mainUrl;
    }

    public String getCheckOutUrl() {
	return checkOutUrl;
    }

    public void setCheckOutUrl(String checkOutUrl) {
	this.checkOutUrl = checkOutUrl;
    }

    public String getLoginID() {
	return loginID;
    }

    public void setLoginID(String loginID) {
	this.loginID = loginID;
    }

    public String getTransactionKey() {
	return transactionKey;
    }

    public void setTransactionKey(String transactionKey) {
	this.transactionKey = transactionKey;
    }

    public String getFromEmail() {
	return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
	this.fromEmail = fromEmail;
    }

    public String getIndexYoutubeUrl() {
	return indexYoutubeUrl;
    }

    public void setIndexYoutubeUrl(String indexYoutubeUrl) {
	this.indexYoutubeUrl = indexYoutubeUrl;
    }

    public String getPaypalSite() {
	return paypalSite;
    }

    public void setPaypalSite(String paypalSite) {
	this.paypalSite = paypalSite;
    }

	public String getAuthorizeEnv() {
		return authorizeEnv;
	}

	public void setAuthorizeEnv(String authorizeEnv) {
		this.authorizeEnv = authorizeEnv;
	}

	public String getNotifyTo() {
		return notifyTo;
	}

	public void setNotifyTo(String notifyTo) {
		this.notifyTo = notifyTo;
	}

	public String getNotifySubject() {
		return notifySubject;
	}

	public void setNotifySubject(String notifySubject) {
		this.notifySubject = notifySubject;
	}
}
