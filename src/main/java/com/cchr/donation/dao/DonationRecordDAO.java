package com.cchr.donation.dao;

import java.util.List;

import com.cchr.donation.model.DonationRecord;

/**
 * @author Zeng
 *
 */
public interface DonationRecordDAO {
	
	/**
     * @param 
     * @return
     */
    public int insertDonationRecord(DonationRecord donationRecord);
    
	/**
     * @param 
     * @return
     */
    public int updateDonationRecord(DonationRecord donationRecord);
    
	/**
     * @param id
     * @return
     */
    public DonationRecord queryDonationRecordById(int id);
    
	/**
     * @param id
     * @return
     */
    public int deleteDonationRecordById(int id);
    
    /**
     * @return
     */
    public List<DonationRecord> getAllDonationRecord();
}
