/**
 * 
 */
package com.cchr.donation.dao;

import java.util.List;

import com.cchr.donation.model.User;

/**
 * @author Fanxin Zeng
 *
 */
public interface UserDAO {
	
	/**
     * �������û�
     * @param user
     * @return
     */
    public int insertUser(User user);
    
    public int updateUser(User user);
    
    public int deleteUser(int id);
    
    public User getUserById(int id);
    
    public User getUserByUserName(String userName);

    public List<User> getAllUser();
}
