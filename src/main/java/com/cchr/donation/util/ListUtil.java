package com.cchr.donation.util;

import org.springframework.util.StringUtils;

public class ListUtil {

	public static String ArrayToString(String[] arr, String split)
	{
		StringBuilder sb = new StringBuilder();
		if(arr!=null && arr.length>0)
		{
			for (int i = 0; i < arr.length; i++) 
			{
				if(!StringUtils.isEmpty(arr[i]))
				{
					sb.append(arr[i]).append(",");
				}
			}
			if(sb.length() > 0)
			{
				sb.delete(sb.length() - 1, sb.length());
			}
		}
		return sb.toString();
	}
}
