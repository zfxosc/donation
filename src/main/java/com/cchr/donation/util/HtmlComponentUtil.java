package com.cchr.donation.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.alibaba.druid.util.StringUtils;
import com.cchr.donation.model.DonationRecord;

public class HtmlComponentUtil {

	public static String getSelect(String id, String name, List<SelectOption> data, String selected, String placeholder) 
	{
		return HtmlComponentUtil.getSelect(id, name, data, selected, placeholder, null);
	}
	
	public static String getSelect(String id, String name, List<SelectOption> data, String selected, String placeholder, String style) 
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<select id=\"").append(id).append("\" name=\"").append(name).append("\" ");
		if(!StringUtils.isEmpty(placeholder))
		{
			sb.append(" data-placeholder=\"").append(placeholder).append("\" ");
		}
		if(!StringUtils.isEmpty(style))
		{
			sb.append(" style=\"").append(style).append("\" ");
		}
		sb.append("\" >");
		if(!StringUtils.isEmpty(placeholder))
		{
			SelectOption op = new SelectOption();
			op.setLabel("");
			op.setValue("");
			generateOption(sb, op, selected);
		}
		if(data != null)
		{
			for(SelectOption op : data)
			{
				generateOption(sb, op, selected);
			}
		}
		sb.append("</select>");
		return sb.toString();
	}
	
	public static List<SelectOption> buildList(String[] source) 
	{
		List<SelectOption> target = new ArrayList<SelectOption>();
    	for (int i = 0; i < source.length; i++) {
    		SelectOption op = new SelectOption();
    		op.setLabel(source[i]);
    		op.setValue(source[i]);
    		target.add(op);
		}
    	return target;
	}
	
	private static void generateOption(StringBuilder sb, SelectOption op, String selected)
	{
		sb.append("<option");
		if(op.getValue() != null)
		{
			sb.append(" value=\"").append(op.getValue()).append("\"");
			if(op.getValue().equals(selected))
			{
				sb.append(" selected=\"selected\"");
			}
		}
		if(op.disabled)
		{
			sb.append(" disabled ");
		}
		sb.append(">");
		if(op.getLabel() != null)
		{
			sb.append(op.getLabel());
		}
		sb.append("</option>");
	}
	
	public static class SelectOption
	{
		String value;
		String label;
		String group;
		boolean disabled = false;
		
		public boolean isDisabled() {
			return disabled;
		}
		public void setDisabled(boolean disabled) {
			this.disabled = disabled;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public String getGroup() {
			return group;
		}
		public void setGroup(String group) {
			this.group = group;
		}
	}
	
	public static StringBuilder initThankyouMailPage(String FIRST_NAME, String DONATION_AMOUNT, String TRANSACTION_DATE,
			String DONOR_FIRST_NAME, String DONOR_LAST_NAME, String PAYMENT_TYPE, String CREDIT_CARD_NO, String TOTAL_GIFT_AMOUNT, 
			DonationRecord donationRecord) throws Exception
	{
		StringBuilder sb  = new StringBuilder();
		ClassPathResource cpr = new ClassPathResource("/mail/cchr-email-thanks.html");
		BufferedReader br = new BufferedReader(new FileReader(cpr.getFile()));
		String line = null;
		while((line = br.readLine()) != null)
		{
			if(line.contains("{TOP_BGCOLOR}"))
			{
				String color = "#2431AB";
				if(!StringUtils.isEmpty(donationRecord.getEffort()) && donationRecord.getEffort().toLowerCase().contains("hdr"))
				{
					color = "#A71931";
				}
				line = line.replace("{TOP_BGCOLOR}", color);
			}
			if(line.contains("{FIRST_NAME}"))
			{
				line = line.replace("{FIRST_NAME}", FIRST_NAME);
			}
			if(line.contains("{DONATION_AMOUNT}"))
			{
				line = line.replace("{DONATION_AMOUNT}", DONATION_AMOUNT);
			}
			if(line.contains("{TRANSACTION_DATE}"))
			{
				line = line.replace("{TRANSACTION_DATE}", TRANSACTION_DATE);
			}
			if(line.contains("{DONOR_FIRST_NAME}"))
			{
				line = line.replace("{DONOR_FIRST_NAME}", DONOR_FIRST_NAME);
			}
			if(line.contains("{DONOR_LAST_NAME}"))
			{
				line = line.replace("{DONOR_LAST_NAME}", DONOR_LAST_NAME);
			}
			if(line.contains("{PAYMENT_TYPE}"))
			{
				// credit card
				if(StringUtils.isEmpty(PAYMENT_TYPE))
				{
					PAYMENT_TYPE = "Credit Card";
				}else{
					PAYMENT_TYPE = "Paypal "+PAYMENT_TYPE;
				}
				line = line.replace("{PAYMENT_TYPE}", PAYMENT_TYPE);
			}
			if(line.contains("{CREDIT_CARD_NO}"))
			{
				// credit card
/*				if(StringUtils.isEmpty(PAYMENT_TYPE) || !PAYMENT_TYPE.equals("paypal"))
				{
					if(!StringUtils.isEmpty(CREDIT_CARD_NO) && CREDIT_CARD_NO.length() > 4)
					{
						StringBuilder s = new StringBuilder();
						char[] chars = CREDIT_CARD_NO.toCharArray();
						for(int i=0; i<chars.length; i++)
						{
							if(i < chars.length - 4)
							{
								s.append("*");
							}
							else
							{
								s.append(chars[i]);
							}
						}
						CREDIT_CARD_NO = s.toString();
					}
					line = line.replace("{CREDIT_CARD_NO}", CREDIT_CARD_NO);
				}
				*/
				if(!StringUtils.isEmpty(CREDIT_CARD_NO)){
					line = line.replace("{CREDIT_CARD_NO}", CREDIT_CARD_NO);
				}
				// paypal
				else 
				{
					continue;
				}
			}
			if(line.contains("{TOTAL_GIFT_AMOUNT}"))
			{
				line = line.replace("{TOTAL_GIFT_AMOUNT}", TOTAL_GIFT_AMOUNT);
			}
			sb.append(line);
		}
		br.close();
		return sb;
	}

}
