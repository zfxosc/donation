package com.cchr.donation.util;

import java.util.ArrayList;
import java.util.List;


public class PageableDataVO<T> {

	private String sEcho;
	private long iTotalRecords;
	private long iTotalDisplayRecords;
	private List<T> aaData;

	public PageableDataVO() {}

	public List<T> getAaData() {
		return aaData;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public long getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(long iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public void addData(T data) {
		if (aaData == null) {
			aaData = new ArrayList<T>();
		}
		aaData.add(data);
	}

	public void addData(List<T> data) {
		if (aaData == null) {
			aaData = new ArrayList<T>();
		}
		aaData.addAll(data);
	}

}
