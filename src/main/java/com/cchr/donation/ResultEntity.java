package com.cchr.donation;

import java.util.List;

public class ResultEntity {

    public static final String SUCCESS_CODE="SUCCESS";
    public static final String FAIL_CODE ="FAILE";
    private String code;
    private List<String> message;
    private String token;
    private String cardNumber;
    private String payPalAccount;
    
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public List<String> getMessage() {
        return message;
    }
    public void setMessage(List<String> message) {
        this.message = message;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getPayPalAccount() {
		return payPalAccount;
	}
	public void setPayPalAccount(String payPalAccount) {
		this.payPalAccount = payPalAccount;
	}      
}
